!
!  ATMS 502 / CSE 566 -- Spring, 2016
!  Program 1:  Linear and nonlinear advection
!  >>>>> Karthik Ramaswamy <<<<<
!
  program pgm2
  implicit none

! ... Definitions ............................................................
! "nx" is the number of physical grid points in x - Not including 'ghost points'
! "ny" is the number of physical grid points in y - Not including 'ghost points' 
! "maxstep" is the maximum number of time steps.
! "c" is [constant] flow speed (e.g. meters/sec) - used only in linear case
! "dx" is grid spacing: distance (e.g. meters) between grid points in X, Y
! "name" is a variable containing your name - used to label the plots.
! 
  integer, parameter      :: nx=61
  integer, parameter      :: ny=61
  integer, parameter      :: maxstep=1000
  real, parameter         :: c=1.0
  real, parameter         :: dx=1.0/real(nx-1)
  real, parameter         :: dy=1.0/real(ny-1)
  character*30, parameter :: name="Karthik Ramaswamy"
! ... Other variables

  real         :: dt,courant,qmax, qmin, rtime, etot, ediss, edisp
  integer      :: i,n,nstep,nplot
  character*1  :: reply,advection_type
! ... Arrays

  real, dimension(1:nx,1:ny)     :: q1,qt, qd
  real, dimension(nx+1,ny)           :: u
  real, dimension(nx,ny+1)           :: v
  real, dimension(maxstep)    :: strace(maxstep) 
  real, dimension(1:nx, 1:ny) :: q_plot
!  real, dimension(nx,maxstep) :: history(nx,maxstep)

! ... Start, print, input ...........................................

  print*,'Program #1    Numerical Fluid Dynamics, Spring 2016'
  print*,' '

  print*,'Enter desired time step:'
  read*,dt
  courant = c*dt/dx

  write(6,1) dt, courant, nint( dx*real(nx) / c / dt )
1 format(' For time step dt = ',f5.3,', courant number = ',f6.3,/, &
         ' Number of time steps for a complete loop  = ',i3)

  print*,'Enter number of time steps to take:'
  read*,nstep

  print*,'Enter plot interval, in steps (1=each step):'
  read*,nplot

  print*,'Enter L for Linear  Lax-Wendroff, U for Linear Upstream advection:'
  read*,advection_type

  
! ... Open the NCAR Graphics package and set colors.
! ... To invert black/white colors, comment out calls to gscr below.

  call opngks
   call gscr(1,0,1.,1.,1.)
   call gscr(1,1,0.,0.,0.)

! ! ... X-axis label

!   call anotat('I','Q',0,0,0,0)

! ... Set default Y plot bounds.
 
!  call agsetf('Y/MINIMUM.',-1.2)
!  call agsetf('Y/MAXIMUM.', 1.2)

! ... Set and plot the initial condition. Note no ghost points passed to plot1d!

  call ic(q1,dx,nx,ny, u,v,q_plot)
  qt = q_plot
  !call stats(q1,nx,ny,0,qmax,qmin)
 ! call contr(q_plot,nx,ny,0.5,0,'Contour plot at initial condition',0,.true.,0,0,0,0,name)

 !call contr(u(1:nx,1:ny),nx,ny,0.01,0,'U Plot',0,.true.,0,0,0,0,name)

  !call contr(v(1:nx, 1:ny),nx,ny,0.01,0,'V Plot',0,.true.,0,0,0,0,name)
!  call plot1d(q1(1:nx),nx,0,qmax,.false.,qtrue(1:nx),'Initial condition',name)



! ... Copy the initial condition (IC) to the "qtrue" array.
!     We use it later since the initial condition is the true final solution.

!  qtrue = q1

! ... Integrate .....................................................

   do n = 1,nstep

!   Set boundary conditions (B.C.s set in advection.f90)
!     call bc(q1,nx)

 !   Compute values at next step
     call  advection(q1,u,v,nx,ny,dx,dy,dt,advection_type)
 !   Do array update at end of time step.
     !call update(q1,q2,nx)
     
 !   Copy latest solution q2(1:nx) to history array.
 !    history(1:nx,n) = q2(1:nx)

! Call contr to plot the current q returned from advection
       rtime = dt*real(n) 
        if (mod(n,100).eq.0) then
           call contr(q1,nx,ny,0.5, rtime,'Plot contour',0,.true.,0,0,0,0,name)
        endif

!    Plot fields when needed.  Again only interior (1:nx) values are plotted.
     !if (n .eq. nstep) then		! code is done; plot final results
!       if (advection_type == 'L') then
!         call contr(q(1:nx),nx,n,qmax,.true. ,qtrue(1:nx),'Final solution',name)
!       else
! !        call plot1d(q2(1:nx),nx,n,qmax,.false.,qtrue(1:nx),'Final solution',name)
!       endif
!      if (mod(n,nplot).eq.0) then	! intermediate plot before end
! !      call plot1d(q2(1:nx),nx,n,qmax,.false.,qtrue(1:nx),'Solution',name)
!     endif

! !   Stats
 call stats(q1,nx,ny,n,qmax, qmin)
    
 call ezy(qmin,n,'Min Q. vs. t')
!     strace(n) = qmax

! !   Check if problem out of bounds i.e. failing.
! !    if (qmax.gt.15) then
!       print*,'Stop - solution blowing up at step',n
! !      call plot1d(q2(1:nx),nx,n,qmax,.false.,qtrue(1:nx),'Solution blowing up',name)
!       nstep = n
! !      exit
! !    endif

! !   end of time loop from n=1 to nstep
  enddo
qd = q1
 
call  erroral(qd,qt,nx, ny, etot, ediss, edisp)
print*,'Total Error = ',etot
print*,'Dissipation Error =', ediss
print*,'Dispersion Error=', edisp


! ! ... Run complete - do final plots .................................

! ! ... Plot Qmax(t)

! !  call agsetf('Y/MINIMUM.',0.)
! !  call agsetf('Y/MAXIMUM.',1.5)
! !  call anotat('N','Max value',0,0,0,0)
! !  call plot1d(strace,nstep,0,qmax,.false.,qtrue(1:nx),'Qmax vs. time',name)

! ! ... Plot history (time-space) surface s(x,t)

! !  call sfc(history,nx,nstep,dt*real(nstep),-90., 0.,    &
! !      	   'Time-Space evolution (view: -90,  0)',name)
! !  call sfc(history,nx,nstep,dt*real(nstep),-75., 5.,    &
! !      	   'Time-Space evolution (view: -75, +5)',name)
! !  call sfc(history,nx,nstep,dt*real(nstep),-30.,20.,    &
! !      	   'Time-Space evolution (view: -30,+20)',name)

! ! ... Close graphics package and stop.

  call clsgks
  stop
  end program pgm2
