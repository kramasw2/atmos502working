! ============================ bc =====================
! BC sets the boundary conditions
! ATMS 502 / CSE 566, Spring 2016
!
! Arguments:
!
!   q1  in/output  real array  values at current time step
!   nx	input      integer     main array size, not including
!                              extra 'ghost' zones/points

  subroutine bc(q1,nx)
  implicit none

  integer, intent(in)                    :: nx
  real, dimension(0:nx+1), intent(inout) :: q1

  print*,' >Insert code for boundary conditions in routine BC.'

  return
  end subroutine bc
