! =========================== sfc =====================
!  sfc plots a 2-d field as a surface and shows the max/min, time & label.
!  NCAR Graphics routine ezsrfc plots the surface.
!  Arguments:
!
!   q      input   real array    field to be displayed.
!   nx,ny  input   integers      dimensions of 'q'
!   time   input   real          time of integration
!   angh   input   real          horizontal viewing angle counter-
!                                  clockwise from x-axis
!   angv   input   real          vertical viewing angle; >0=above plane
!                                  of field average.
!   label  input   character*(*) character label; should not
!                                  exceed 30 characters long.
!   name   input   character*(*) name to place on plot
!
    subroutine sfc(q,nx,ny,time,angh,angv,label,name)
    implicit none

    integer, intent(in)                :: nx,ny
    real, intent(in)                   :: angh,angv,time
    real, dimension(nx,ny), intent(in) :: q
    character*(*), intent(in)          :: label,name

    integer                            :: i,j,mmlen
    real                               :: qmin,qmax
    real, allocatable, dimension(:)    :: work
    character*15                       :: tlabel
    character*45                       :: mmlabel

! ... Allocate work array for use by graphics routine

    allocate(work(2*nx*ny+nx+ny))

! ... Find min and max of arrays to be plotted and set up labels

    qmax = maxval(q)
    qmin = minval(q)
!   write(*,*) 'Max Value = ',qmax,' Min value = ',qmin

! ... Write labels into character strings

    write(tlabel,1) time
1   format('TIME =',f7.4)

    if (abs(qmin).ge.100.0.or. abs(qmax).ge.100.0) then
      write(mmlabel,2) qmin,qmax
2     format('MIN = ',e13.4,'  MAX = ',e13.4)
      mmlen = 40

    else
      write(mmlabel,3) qmin,qmax
3     format('MIN=',f7.3,'  MAX=',f6.3)
      mmlen = 23
    endif

! ... Plot labels

    call set(0.,1.,0.,1.,0.,1.,0.,1.,1)
    call pcmequ(0.50,0.97,  label         ,0.020,0.0, 0.0)
    call pcmequ(0.95,0.02,mmlabel(1:mmlen),0.015,0.0, 1.0)
    call pcmequ(0.05,0.02, tlabel         ,0.015,0.0,-1.0)

! ... Additional labels

    call pcmequ(0.02,0.99,name,0.01,90.,1.)
    call pcmequ(0.98,0.06,'ATMS 502/CSE 566',0.01,0.,1.)
    call pcmequ(0.02,0.06,'Spring, 2016',0.01,0.,-1.)

! ... Call EZSRFC to show the data array
 
    call ezsrfc(q,nx,ny,angh,angv,work)

    deallocate(work)
    return

    end subroutine sfc
