program test
implicit none

integer, parameter :: nx=121, ny=121
real :: dx, dy, dt, pi
real, dimension(nx,ny) :: q1,q_plot
real, parameter :: r=0.130, x0=0.0, y0=0.30
character :: label = 'initial condition'

real 	:: rtime,cint,angh,angv
integer :: n,colors
logical	:: pltzero
  real, dimension(1:nx+1,1:ny)   :: u
  real, dimension(1:nx,1:ny+1)   :: v


dy=1.0/(real(nx-1))
dx=dy
pi = 4.0*atan(1.0)
dt = pi/600


    
call ic(q1,dx,nx,ny,u,v,q_plot)
	cint    = 1.0	
    rtime   = 0 !dt*real(n)
    colors  = 0
    pltzero = .true.
    print*,'Plotting contours.'
    call opngks
   ! write(label,'(''Contour plot at n = '',i2)') n 
    call contr(q1,nx,ny,cint,rtime,'initial condition', &
               colors,pltzero,0,0,0,0,'Sooraj')
call clsgks

! angh = -45.0
!     angv =  20.0
!     print*,'Plotting surface.'
!     write(label,'(''Surface plot at n = '',i2)') n
!     call sfc(q1,nx,ny,rtime,angh,angv,'initial condition','Sooraj')

    
end program test
