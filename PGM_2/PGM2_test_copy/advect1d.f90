
! ======================= advect1d  ===================
! Integrate forward (advection only) by one time step.
! ATMS 502 / CSE 566, Spring 2016
!
! Arguments:
!
!   q_1d	   input    real array   values at current time step
!   q2_1d          output   real array   values at next time step
!   vel_1d         input    real array   velocity values at current ts (u/v)
!   dx             input    real         grid spacing
!   dt             input    real         time step
!   nx             input    integer	 number of grid points
!   advection_type input    character    'L' if Lax Wendroff, 'U' if Upstream
  subroutine advect1d(q_1d,q2_1d,vel_1d,dt,dx,dy,nx,ny,advection_type)
  implicit none

  integer, intent(in)                  :: nx,ny
  real, intent(in)                     :: dx,dy,dt
  real, dimension(0:nx+1), intent(inout)  :: q_1d
  real, dimension(0:nx+1), intent(out) :: q2_1d
  character, intent(in)                :: advection_type
  real, dimension(1:nx+1), intent(in)              :: vel_1d
  integer :: i
  real    :: courant,c

! Lax Wendroff 1D advection ------------------------------------
  if (advection_type == 'L') then 
    do i = 1,nx
      c = 0.5*(vel_1d(i) + vel_1d(i+1)) ! Averaging vels at current and next pt
      courant = c*dt/dx
      q2_1d(i) = q_1d(i) - courant*0.5*(q_1d(i+1) - q_1d(i-1)) + (courant**2)*0.5*(q_1d(i+1) - 2*q_1d(i) + q_1d(i-1))
    end do

! Upstream 1D Advection ----------------------------------------
  else if (advection_type == 'U') then
    do i = 1,nx
      c = 0.5*(vel_1d(i) + vel_1d(i+1)) ! Averaging vels at current and next pt
      courant = c*dt/dx
      if (courant.gt.0) then
        q2_1d(i) = q_1d(i) - courant*(q_1d(i) - q_1d(i-1))
      else
        q2_1d(i) = q_1d(i) - courant*(q_1d(i+1) - q_1d(i))
      end if
    end do

  else
    print*,' >Advection: unknown advection type ',advection_type
    stop
  endif

  return
  end subroutine advect1d
