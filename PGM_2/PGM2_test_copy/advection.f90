! ======================= advection ===================
! Integrate forward (advection only) by one time step.
! ATMS 502 / CSE 566, Spring 2016
!
! Arguments:
!
!   q1	           input    real array   values at current time step
!   q2	           output   real array   values at next time step
!   c              input    real         flow speed for linear case
!   dx,dy          input    real         grid spacing
!   dt             input    real         time step
!   nx,ny             input    integer	 number of grid points
!   advection_type input    character    'L' if linear, 'N' if nonlinear
!   u,v            input    real array   values at current ts

  subroutine advection(q1,u,v,nx,ny,dx,dy,dt,advection_type)
  implicit none

  integer, intent(in)                  :: nx,ny
  real, intent(in)                     :: dx,dy,dt
  real, dimension(1:nx,1:ny), intent(inout)  :: q1
  character, intent(in)                :: advection_type
  real, dimension(1:nx+1, 1:ny), intent(in)       :: u
  real, dimension(1:nx, 1:ny+1), intent(in)        :: v
  real, dimension(0:nx+1)                ::q_1d, q2_1d
  real, dimension(1:nx+1)                ::vel_1d
  integer :: i,j

  ! 1D advection in x 
  do i = 1,ny
     q_1d(1:nx) = q1(:,i)
     vel_1d(1:nx+1) = u(:,i)
  ! Add Boundary Conditions (ghost points)
     q_1d(0) = q_1d(1)
     q_1d(nx+1) = q_1d(nx)
     
     call  advect1d(q_1d,q2_1d,vel_1d,dt,dx,dy,nx,ny,advection_type)
     q1(:,i) = q2_1d(1:nx)
  enddo

 ! 1D advection in y for the entire q matrix
  do j = 1,nx
     q_1d(1:ny) = q1(j,:)
     vel_1d(1:ny+1) = v(j,:)
  ! Add Boundary Conditions (ghost points)
     q_1d(0) = q_1d(1)
     q_1d(ny+1) = q_1d(ny)
     
  ! 1D advection using advect1d
     call  advect1d(q_1d,q2_1d, vel_1d,dt,dx,dy,nx,ny,advection_type)
     q1(j,:) = q2_1d(1:ny)
  enddo

  return
  end subroutine advection
