ATMS 502 / CSE 566   --   Program #2, Spring 2016

TO use the make_images and make_viewer routines:

1) modify make_images to correspond to your data.
   You will need to modify 2 things:

   	a) set plot_names   = ( contr sfc )

   in this example you call contr and then call sfc - repeatedly
   if your order is reversed, change the above.

   then change:

	b) setenv first_plot  4
 
   Does the first of a series of (contr,sfc ... or sfc,contr) plots
   start with the 4th plot, or the first?  If the first, change to "1".

2) run "make_images gmeta" (or if you have renamed gmeta, give it that name)

   You should get a large group of .gif files.

3) run "make_viewer tar" to get a tar file, or "make_viewer zip" for a zip file.

4) Then transfer the tar/zip file back to your PC.  Extract the files.

5) Then open "viewer2p.shtml" with a browser.
