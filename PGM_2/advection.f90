! ======================= advection ===================
! Integrate forward (advection only) by one time step.
! ATMS 502 / CSE 566, Spring 2016
!
! Arguments:
!
!   q1	           input    real array   values at current time step
!   q2	           output   real array   values at next time step
!   c              input    real         flow speed for linear case
!   dx             input    real         grid spacing
!   dt             input    real         time step
!   nx             input    integer	 number of grid points
!   advection_type input    character    'L' if linear, 'N' if nonlinear

  subroutine advection(q1,q2,c,dx,dt,nx,advection_type)
  implicit none

  integer, intent(in)                  :: nx
  real, intent(in)                     :: c,dx,dt
  real, dimension(0:nx+1), intent(in)  :: q1
  real, dimension(0:nx+1), intent(out) :: q2
  character, intent(in)                :: advection_type

  integer :: i
  real    :: courant

  if (advection_type == 'L') then
    courant = c*dt/dx
    !print*,' >Put advection code here for linear advection.'
    do i = 1,nx
      q2(i) = q1(i) - courant*0.5*(q1(i+1) - q1(i-1)) + courant*courant*0.5*(q1(i+1) - 2*q1(i) +q1(i-1))
    end do

  else if (advection_type == 'N') then
    ! print*,' >Put advection code here for nonlinear advection.'
    do i = 1,nx
      courant = q1(i)*dt/dx
      q2(i) = q1(i) - courant*0.5*(q1(i+1) - q1(i-1)) + courant*courant*0.5*(q1(i+1) - 2*q1(i) +q1(i-1))
    end do

  else
    print*,' >Advection: unknown advection type ',advection_type
    stop
  endif

  return
  end subroutine advection
