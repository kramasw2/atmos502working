! ========================= update ====================
! Update: replace old values with new ones
! We are NOT copying ghost points here.
! ATMS 502 / CSE 566, Spring 2016
!
! Arguments:
!
!   q1,q2   in/output   real arrays    old, new data arrays
!   nx      input       integer        size of arrays
!

  subroutine update(q1,q2,nx)
  implicit none

  integer, intent(in)                 :: nx
  real,dimension(0:nx+1), intent(out) :: q1
  real,dimension(0:nx+1), intent(in)  :: q2

  q1(1:nx) = q2(1:nx)

  return

  end subroutine update
