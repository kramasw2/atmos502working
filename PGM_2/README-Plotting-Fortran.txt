!
! Examples of calling the contour ("contr") and surface ("sfc") routines
!
! Variables you'll need:
!
    real 	:: rtime,cint,angh,angv
    integer 	:: n,colors
    logical	:: pltzero
!
! Plot contours
! n is our do-loop counter in the full code
! cint is contour interval ( 5.0 = plot contours at -10, -5, +5, +10, ... )
! rtime is the integration time
! colors is the color setting in the contr routine
! pltzero, if false, skips plotting the zero contour (recommended)
!
    cint    = 5.0	
    rtime   = dt*real(n)
    colors  = 0
    pltzero = .false.
    print*,'Plotting contours.'
    write(label,'(''Contour plot at n = '',i2)') n
    call contr(q,nx,ny,cint,rtime,label, &
               colors,pltzero,0,0,0,0,name)
!
! Plot surface
! angh is horizontal viewing angle, degrees, counter-clockwise from X-axis
! angv is vertical   viewing angle above x-y plane
!
    angh = -45.0
    angv =  20.0
    print*,'Plotting surface.'
    write(label,'(''Surface plot at n = '',i2)') n
    call sfc(q,nx,ny,rtime,angh,angv,label,name)
