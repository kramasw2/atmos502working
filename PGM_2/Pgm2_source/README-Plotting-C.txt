/*
 * examples of calling the contour ("contr") and surface ("sfc") routines
 */

	int colors;
	char label[25];
	float cint;
	n = 10;			/* our time step counter in the full code */
/*
 * Plot contours
 * n is our do-loop counter in the full code
 * cint is contour interval ( 5.0 = plot contours at -10, -5, +5, +10, ... )
 * rtime is the integration time
 * colors is the color setting in the contr routine, 0=default
 * pltzero, if false, skips plotting the zero contour (recommended)
 */

	cint    = 5.0;	/* contour interval */
	colors  =   0;	/* positive values colored red, negative blue, all solid */
	pltzero =  -1;	/* don't plot zero line */
	printf("Plotting contours.\n");
	sprintf(label,"Contour plot at n = %d",n);
	contr(q,NX,NY,cint,rtime,label,colors,pltzero,0,0,0,0,name);

/*
 * Plot surface
 * angh is horizontal viewing angle, degrees, counter-clockwise from X-axis
 * angv is vertical   viewing angle above x-y plane
 */

	angh = -45.0;
	angv =  20.0;
	printf("Plotting surface.\n");
	sprintf(label,"Surface plot at n = %d",n);
	sfc(q,NX,NY,rtime,angh,angv,label,name);
