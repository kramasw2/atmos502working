!
!  ATMS 502 / CSE 566 -- Spring, 2016
!  Program 1:  Linear and nonlinear advection
!  >>>>> Karthik Ramaswamy <<<<<
!
  program pgm1
  implicit none

! ... Definitions ............................................................
! "nx" is the number of physical grid points - Not including 'ghost points'
! "maxstep" is the maximum number of time steps.
! "c" is [constant] flow speed (e.g. meters/sec) - used only in linear case
! "dx" is grid spacing: distance (e.g. meters) between grid points in X, Y
! "name" is a variable containing your name - used to label the plots.

  integer, parameter      :: nx=75
  integer, parameter      :: maxstep=200
  real, parameter         :: c=1.0
  real, parameter         :: dx=0.1
  character*30, parameter :: name="Karthik Ramaswamy"

! ... Other variables

  real         :: dt,courant,qmax
  integer      :: i,n,nstep,nplot
  character*1  :: reply,advection_type

! ... Arrays

  real, dimension(0:nx+1)     :: q1,q2,qtrue
  real, dimension(maxstep)    :: strace(maxstep)
  real, dimension(nx,maxstep) :: history(nx,maxstep)

! ... Start, print, input ...........................................

  print*,'Program #1    Numerical Fluid Dynamics, Spring 2016'
  print*,' '

  print*,'Enter desired time step:'
  read*,dt
  courant = c*dt/dx

  write(6,1) dt, courant, nint( dx*real(nx) / c / dt )
1 format(' For time step dt = ',f5.3,', courant number = ',f6.3,/, &
         ' Number of time steps for a complete loop  = ',i3)

  print*,'Enter number of time steps to take:'
  read*,nstep

  print*,'Enter plot interval, in steps (1=each step):'
  read*,nplot

  print*,'Enter L for linear, N for nonlinear advection:'
  read*,advection_type

! ... Open the NCAR Graphics package and set colors.
! ... To invert black/white colors, comment out calls to gscr below.

  call opngks
  call gscr(1,0,1.,1.,1.)
  call gscr(1,1,0.,0.,0.)

! ... X-axis label

  call anotat('I','Q',0,0,0,0)

! ... Set default Y plot bounds.
 
  call agsetf('Y/MINIMUM.',-1.2)
  call agsetf('Y/MAXIMUM.', 1.2)

! ... Set and plot the initial condition. Note no ghost points passed to plot1d!

  call ic(q1,dx,nx)
  call stats(q1,nx,0,qmax)
  call plot1d(q1(1:nx),nx,0,qmax,.false.,qtrue(1:nx),'Initial condition',name)

! ... Copy the initial condition (IC) to the "qtrue" array.
!     We use it later since the initial condition is the true final solution.

  qtrue = q1

! ... Integrate .....................................................

  do n = 1,nstep

!   Set boundary conditions
    call bc(q1,nx)

!   Compute values at next step
    call advection(q1,q2,c,dx,dt,nx,advection_type)

!   Do array update at end of time step.
    call update(q1,q2,nx)

!   Copy latest solution q2(1:nx) to history array.
    history(1:nx,n) = q2(1:nx)

!   Plot fields when needed.  Again only interior (1:nx) values are plotted.
    if (n .eq. nstep) then		! code is done; plot final results
      if (advection_type == 'L') then
        call plot1d(q2(1:nx),nx,n,qmax,.true. ,qtrue(1:nx),'Final solution',name)
      else
        call plot1d(q2(1:nx),nx,n,qmax,.false.,qtrue(1:nx),'Final solution',name)
      endif
    else if (mod(n,nplot).eq.0) then	! intermediate plot before end
      call plot1d(q2(1:nx),nx,n,qmax,.false.,qtrue(1:nx),'Solution',name)
    endif

!   Stats
    call stats(q2,nx,n,qmax)
    strace(n) = qmax

!   Check if problem out of bounds i.e. failing.
    if (qmax.gt.1.5) then
      print*,'Stop - solution blowing up at step',n
      call plot1d(q2(1:nx),nx,n,qmax,.false.,qtrue(1:nx),'Solution blowing up',name)
      nstep = n
      exit
    endif

!   end of time loop from n=1 to nstep
  enddo

! ... Run complete - do final plots .................................

! ... Plot Qmax(t)

  call agsetf('Y/MINIMUM.',0.)
  call agsetf('Y/MAXIMUM.',1.5)
  call anotat('N','Max value',0,0,0,0)
  call plot1d(strace,nstep,0,qmax,.false.,qtrue(1:nx),'Qmax vs. time',name)

! ... Plot history (time-space) surface s(x,t)

  call sfc(history,nx,nstep,dt*real(nstep),-90., 0.,    &
      	   'Time-Space evolution (view: -90,  0)',name)
  call sfc(history,nx,nstep,dt*real(nstep),-75., 5.,    &
      	   'Time-Space evolution (view: -75, +5)',name)
  call sfc(history,nx,nstep,dt*real(nstep),-30.,20.,    &
      	   'Time-Space evolution (view: -30,+20)',name)

! ... Close graphics package and stop.

  call clsgks
  stop
  end program pgm1
