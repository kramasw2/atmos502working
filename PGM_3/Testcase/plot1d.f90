! ========================= plot1d ====================
! Plot1d plots a one-dimensional function using NCAR graphics
! ATMS 502 / CSE 566, Spring 2016
!
! Arguments (all are input):
!
!   q	     real array   array to plot (important: only pass that
!                         part of the array to plot - NO boundary points)
!   nx	     integer      size of array
!   n	     integer      time step counter, from main program
!   smax     real         max absolute value in s array - used in label
!   overlay  logical      if nonzero, the plot will be an overlay of 2 fields
!   qtrue    real array   [optional] overlay field, ignored if overlay is false
!   title    char*(*)     title (time step, max info appended to this)
!   name     char*(*)     name to place on plot
!
    subroutine plot1d(q,nx,n,smax,overlay,qtrue,title,name)
    implicit none

    integer, intent(in)               :: nx,n
    real, intent(in)                  :: smax
    real, dimension(nx), intent(in)   :: q,qtrue
    logical, intent(in)               :: overlay
    character*(*), intent(in)         :: title,name

    integer                           :: i
    real, allocatable, dimension(:,:) :: both
    character*200                     :: label
    character*16                      :: AGDSHN

    write(label,1) title,n,smax
1   format(a,': N=',i3,' Max=',f8.5)

    write(6,2) label(1:(19+len(title)))
2   format('Plotting ',a)

!   suppress automatic graphics frame advance for now
    call agseti('frame.',2)

    if (.not.overlay) then

      call ezy(q,nx,label)

    else

      allocate(both(nx,2))
      do i = 1,nx
        both(i,1) = q(i)
        both(i,2) = qtrue(i)
      enddo
      call agsetr('DASH/SELECTOR.',2.)
      call agseti('DASH/LENGTH.'  ,20)
      call agsetc(AGDSHN(1),'$$$$$$$$$$$$$$$$$$$$')
      call agsetc(AGDSHN(2),'$$''''$$''''$$''''$$''''$$''''')
      call ezmy(both,nx,2,nx,label)
      deallocate(both)
    endif

! ... add additional labels

    call set(.05,.95,.05,.95,0.,1.,0.,1.,1)
    call pcmequ(0.02,1.0,name,0.01,90.,1.)
    call pcmequ(1.0,0.05,'ATMS 502/CSE 566',0.01,0.,1.)
    call pcmequ(0.0,0.05,'Spring 2016',0.01,0.,-1.)

! ... done

    call frame
!   restore automatic graphics frame advance
    call agseti('frame.',1)
    return

    end subroutine plot1d
