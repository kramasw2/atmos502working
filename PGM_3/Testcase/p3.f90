!
!  ATMS 502 / CSE 566 -- Spring, 2016
!  Program 1:  Linear and nonlinear advection
!  >>>>> Karthik Ramaswamy <<<<<
!
  program pgm3
  implicit none

! ... Definitions ............................................................
! "nx" is the number of physical grid points in x - Not including 'ghost points'
! "ny" is the number of physical grid points in y - Not including 'ghost points' 
! "maxstep" is the maximum number of time steps.
! "c" is [constant] flow speed (e.g. meters/sec) - used only in linear case
! "dx" is grid spacing: distance (e.g. meters) between grid points in X, Y
! "name" is a variable containing your name - used to label the plots.
!"qt" is the true solution (initial condition)
! "qd" is the approximated solution (either L.W or Upstream)
! "gpoints" is the number of ghostpoints passed to advection
! 
  integer, parameter      :: nx=61
  integer, parameter      :: ny=61
  integer, parameter      :: maxstep=1000
  integer, parameter      :: gpoints = 3
  real, parameter         :: c=1.0
  real, parameter         :: dx=1.0/real(nx-1)
  real, parameter         :: dy=1.0/real(ny-1)
  character*30, parameter :: name="Karthik Ramaswamy"
! ... Other variables

  real    :: dt,courant,qmax, qmin, rtime, etot, ediss, edisp, rho, pi,angh, angv
  integer      :: i,j,n,nplot, nstep
  character*1  :: reply,advection_type, label
! ... Arrays

  real, dimension(1:nx,1:ny)     :: q1,qt,qd
  real, dimension(nx+1,ny)           :: u
  real, dimension(nx,ny+1)           :: v
  real, dimension(maxstep)    :: strace(maxstep) 
  real, dimension(1:nx, 1:ny) :: q_plot
  real,dimension(1:maxstep)     :: qmin_ar, qmax_ar
!  real, dimension(nx,maxstep) :: history(nx,maxstep)

! ... Start, print, input ...........................................

  print*,'Program #1    Numerical Fluid Dynamics, Spring 2016'
  print*,' '

  print*,'Enter Desired Time step:'
  read*, dt
  print*,'Enter number of time steps to take:'
  read*,nstep

  courant = c*dt/dx

 write(6,1) dt, courant, nint( dx*real(nx) / c / dt )
  
1 format(' For time step dt = ',f5.3,', courant number = ',f6.3,/, &
         ' Number of time steps for a complete loop  = ',i3)


  print*,'Enter plot interval, in steps (1=each step):'
  read*,nplot

  print*,'Enter L for Linear  Lax-Wendroff, C for 6th Order Crowley, T for Takacs  advection:'
  read*,advection_type

  
! ... Open the NCAR Graphics package and set colors.
! ... To invert black/white colors, comment out calls to gscr below.

  call opngks
   call gscr(1,0,1.,1.,1.)
   call gscr(1,1,0.,0.,0.)

! ! ... X-axis label

!   call anotat('I','Q',0,0,0,0)

! ... Set default Y plot bounds.
 
!  call agsetf('Y/MINIMUM.',-1.2)
!  call agsetf('Y/MAXIMUM.', 1.2)

! ... Set and plot the initial condition. Note no ghost points passed to plot1d!

  call ic(q1,dx,nx,ny, u,v,q_plot)
! Initial condition plots as contrs
!  call contr(q1,nx,ny,2.0, rtime,'Initial Condition',0,.false.,0,0,0,0,name)
!  call contr(u,nx+1,ny,0.1, rtime,'Initial Condition',0,.false.,0,0,0,0,name)
!  call contr(v,nx,ny+1,0.1, rtime,'Initial Condition',0,.false.,0,0,0,0,name)

! This copies the initial condition to the qt array 
! qt = q_plot
 !call stats(q1,nx,ny,0,qmax,qmin)

! ... Integrate .....................................................

   do n = 1,nstep

!!   Set boundary conditions (B.C.s set in advection.f90)
!     call bc(q1,nx)

 !   Compute values at next step - Calling the advection routine
    call  advection(q1,u,v,nx,ny,dx,dy,dt,advection_type, gpoints)
 !   Do array update at end of time step: This is done within advection.f90.
     !call update(q1,q2,nx)
! Call contr to plot the current q returned from advection
       rtime = dt*real(n) 
       if (mod(n,25).eq.0) then
           call contr(q1,nx,ny,2.0, rtime,'Q field at time T',0,.false.,0,0,0,0,name)
        endif
!    Plot fields when needed.  Again only interior (1:nx) values are plotted.
     !if (n .eq. nstep) then		! code is done; plot final results
!       if (advection_type == 'L') then
!         call contr(q(1:nx),nx,n,qmax,.true. ,qtrue(1:nx),'Final solution',name)
!       else
! !        call plot1d(q2(1:nx),nx,n,qmax,.false.,qtrue(1:nx),'Final solution',name)
!       endif
!      if (mod(n,nplot).eq.0) then	! intermediate plot before end
! !      call plot1d(q2(1:nx),nx,n,qmax,.false.,qtrue(1:nx),'Solution',name)
!     endif

! !   Stats - to extract qmax and qmin
 !call stats(q1,nx,ny,n,qmax, qmin)
!     strace(n) = qmax
 !qmin_ar(n) = qmin
 !qmax_ar(n) = qmax
! !   Check if problem out of bounds i.e. failing.
! !    if (qmax.gt.15) then
!       print*,'Stop - solution blowing up at step',n
! !      call plot1d(q2(1:nx),nx,n,qmax,.false.,qtrue(1:nx),'Solution blowing up',name)
!       nstep = n
! !      exit
! !    endif

! !   end of time loop from n=1 to nstep
  enddo

! Stores the final solution in qd to pass to the takacs erroranalysis function, erroral
 !qd = q1
 
! call  erroral(qd,qt,nx, ny, etot, ediss, edisp, rho)
!print*,'Total Error = ',etot
!print*,'Dissipation Error =', ediss
!print*,'Dispersion Error=', edisp
!print*,'Max correlation=', rho

! ! ... Run complete - do final plots .................................

! ! ... Plot Qmax(t)


!call ezy(qmin_ar(1:nstep),n,'Min Q. vs. t')
!call ezy(qmax_ar(1:nstep),n,'Max Q. vs. t')


!angh = -45.0
!angv =  20.0
!print*,'Plotting surface.'
!!write(label,'(''Surface plot at n = '',i2)') n
!call sfc(q1,nx,ny,rtime,angh,angv,label,name)

! !  call agsetf('Y/MINIMUM.',0.)
! !  call agsetf('Y/MAXIMUM.',1.5)
! !  call anotat('N','Max value',0,0,0,0)
! !  call plot1d(strace,nstep,0,qmax,.false.,qtrue(1:nx),'Qmax vs. time',name)

! ! ... Plot history (time-space) surface s(x,t)

! !  call sfc(history,nx,nstep,dt*real(nstep),-90., 0.,    &
! !      	   'Time-Space evolution (view: -90,  0)',name)
! !  call sfc(history,nx,nstep,dt*real(nstep),-75., 5.,    &
! !      	   'Time-Space evolution (view: -75, +5)',name)
! !  call sfc(history,nx,nstep,dt*real(nstep),-30.,20.,    &
! !      	   'Time-Space evolution (view: -30,+20)',name)

! ! ... Close graphics package and stop.

  call clsgks
  stop
  end program pgm3
