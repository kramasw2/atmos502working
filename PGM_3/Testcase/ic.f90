! ============================ ic =====================
! IC sets the initial condition
! ATMS 502 / CSE 566, Spring 2016
!
! Arguments:
!
!   q1  output  real array   holds initial condition
!   dx  input   real         grid spacing
!   nx  integer              number of grid points

  subroutine ic(q1,dx,nx,ny,u,v,q_plot)
  implicit none

  integer, intent(in)                          :: nx,ny
  real, intent(in)                             :: dx!,dy
  real, dimension(1:nx, 1:ny), intent(out)     :: q1
  real, dimension(1:nx, 1:ny), intent(out)     :: q_plot 
  real, dimension(1:nx+1, 1:ny), intent(out)     :: u
  real, dimension(1:nx, 1:ny+1)                 :: v
  real                                         :: x,y
  real, parameter                              ::x_0 = 0.0, y_0 = -0.05
  integer :: i
  integer :: j
  real    :: pi,r,d
  r = 0.165
   
  pi = 4.0*atan(1.0)
!  length = dx*real(nx)

! Loop Generating 'q'
  do i = 1,nx
       do j = 1,ny
          x = dx*real(i-1)-0.5
          y = dx*real(j-1)-0.5           
          d = sqrt((x - x_0)**2 + (y - y_0)**2)
          if (d.gt.r) then
            q1(i,j) = 0.0
          else 
            q1(i,j) = 5*(1 + cos(pi*d/r))
          end if
       enddo
 enddo

! Loop Generating 'u'
do i = 1,nx+1
    do j = 1,ny
      x = (dx*real(i-1) -0.5 - dx/2) 
      y =  (dx*real(j-1) -0.5)
      u(i,j) = (sin(4*pi*x)*sin(4*pi*y))
    enddo
enddo

! Loop Generating 'v'
do i = 1,nx
   do j = 1,ny+1
      x = (dx*real(i-1) -0.5) 
      y =  (dx*real(j-1) -0.5 - dx/2)
     v(i,j) = cos(4*pi*x)*cos(4*pi*y)
   enddo
enddo

q_plot = q1(1:nx,1:ny)
return
  end subroutine ic
