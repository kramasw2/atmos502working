
! ========================= erroral====================
! Update: replace old values with new ones
! We are NOT copying ghost points here.
! ATMS 502 / CSE 566, Spring 2016
!
! Arguments:
!
!   qd,qt   in/output   real arrays    true, differential approx, data arrays
!   nx,ny      input       integer        size of arrays
!   

subroutine erroral(qd,qt,nx, ny, etot, ediss, edisp,rho)
implicit none

integer, intent(in)                 :: nx,ny
real,dimension(1:nx, 1:ny), intent(in) :: qd
real,dimension(1:nx, 1:ny), intent(in)  :: qt
real :: qt_bar, qd_bar, rho_n, rho_d1, rho_d2, sig
real, intent(out)  ::etot, ediss, edisp, rho 
integer :: i,j

! Computing averages
qt_bar = sum(qt)/(nx*ny)
qd_bar = sum(qd)/(nx*ny)
sig = sum(((qt - qd)- sum(qt - qd)/(nx*ny))**2)/(nx*ny)


!Total  Error Analysis from Takacs
etot = 0.0
do i = 1,nx
   do j = 1,ny
      etot = etot +  ((qt(i,j) - qd(i,j))**2)
   end do
end do

etot = etot/(nx*ny)

! Compute rho, dissipation and dispersion errors

rho_n = 0
rho_d1 = 0
rho_d2 = 0

do i = 1,nx
   do j = 1,ny
      rho_n = rho_n + (qd(i,j) - qd_bar)*(qt(i,j) - qt_bar)
      rho_d1 = rho_d1 + (qd(i,j) - qd_bar)**2
      rho_d2 = rho_d2 + (qt(i,j) - qt_bar)**2
   end do
end do

rho = rho_n/(sqrt(rho_d1*rho_d2))


! Dispersion error
edisp = 2*(1-rho)*(sqrt(sum((qt-sum(qt)/(nx*ny))**2)/(nx*ny)))*(sqrt(sum((qd-sum(qd)/(nx*ny))**2)/(nx*ny)))

! Dissipation Error
ediss = etot - edisp

!edisp = (sqrt(sum((qt-sum(qt))**2)/(nx*ny)) - sqrt(sum((qd-sum(qd))**2)/(nx*ny)))**2 + (qt_bar - qd_bar)**2

! Dispersion error
!edisp = etot - ediss

return

end subroutine erroral
