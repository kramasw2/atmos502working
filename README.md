# README #

![sL50.png](https://bitbucket.org/repo/oXakea/images/1443942437-sL50.png)

Image shows evolution of a cone in a deformational 2D flow field; simple advection diffusion case. 

This is an active repo I'm using to build a 3D unsteady Advection Diffusion solver for a graduate level class. 

### What is this repository for? ###

* Contains programs (and associated subroutines) pertaining to different problem statements
* The older programs build up to a 3D quasi compressible unsteady solver

### How do I get set up? ###

* Instructions coming here soon; I'm just sorting out some older programs for now and unifying file/folder structures