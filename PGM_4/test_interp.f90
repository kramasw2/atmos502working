!  Test program for dointerp / contr
!  ATMS 502 / CSE 566
!  Brian Jewett	 Spring 2016
!
	program dotest
	implicit none
!
! ... Dimensions
!
	integer, parameter :: nx=91,ny=nx
	integer 	   :: n
!
! ... Definitions
!
	logical :: pltzero
	integer :: i,j,colors,flag,ratio, bcwidth,		&
		   nestsizeX, nestsizeY,			&
      	           nestX1   ,nestX2   ,nestY1   ,nestY2   ,	&
      	  	   nestX1old,nestX2old,nestY1old,nestY2old
	real    :: cint,rtime,dx,				&
      	  	   s1coarse(0:nx+1,0:ny+1), 			&
      	  	   s1BCtest(0:nx+1,0:ny+1), 			&
		   s1nest(0:nx+1,0:ny+1)
	character*15 :: name
        parameter (name="Test_interp")
!
! ... Open the graphics package and invert black/white default colors
!
        call opngks
        call gscr(1,0,1.,1.,1.)
        call gscr(1,1,0.,0.,0.)
!
! ... This case uses one ghost point, with arrays(0:nx+1) - set bcwidth
!
        bcwidth = 1
!
! ... Make up a test initial condition:
!
	n = 0
	rtime = 0.
	do j = 1,ny
	  do i = 1,nx
	      s1coarse(i,j) = -1000./ 			&
      	        ( 0.01 + real(i-nx/3)**2		&
      	             + real(j-ny/2)**2			&
      	             + real(j-ny/4)*real(i-nx/4) )	&
      		       + 1000./ 			&
      	        ( 0.01 + real(i-2*nx/3)**2		&
      	             + real(j-3*ny/5)**2		&
      	             + real(j-ny/2)*real(i-nx/2) )
	      s1BCtest(i,j) = -500.0
	    enddo
	  enddo
!
! Plot coarse grid
!
	cint    = 1.0
	colors  = 0
	pltzero = .true.

	call contr(s1coarse(1:nx,1:ny),nx,ny,cint,rtime, &
      	  'Initial coarse grid',colors,pltzero,0,0,0,0,name)
!
! Determine nest size, place it in the center of the domain, and plot.
!
	print*,'Enter nest refinement ratio'
	read*,ratio

! Size of the nest in coarse grid coordinates
! "Size" here is really width.  The number
! of points in either direction is 1 more.

	nestsizeX = (nx-1)/ratio
	nestX1 = (nx/2) - nestsizeX/2
	nestX2 = nestX1 + nestsizeX

	nestsizeY = (ny-1)/ratio
	nestY1 = (ny/2) - nestsizeY/2
	nestY2 = nestY1 + nestsizeY

	call contr(s1coarse(1:nx,1:ny),nx,ny,cint,rtime,	&
      	  'Coarse grid w/nest box',colors,pltzero,		&
      	   nestX1,nestX2,nestY1,nestY2,name)
!
! INITIALIZE NEST:  Interp. to this position (flag=1), and plot the nest.
! Note when calling dointerp that you must also pass the number of ghost
! points - here, since arrays are defined 0:nx+1, the bcwidth=1. 
! So: do NOT call dointerp with "array(1:nx,1:ny)" or the like.
! 'nestX1old' and such (last four arguments to dointerp) are ignored when
! placing (initializing) a nest.
!
	flag = 1
	call dointerp(s1coarse,s1nest,nx,ny,bcwidth, &
      	  nestX1,nestX2,nestY1,nestY2,n,ratio,flag,0,0,0,0)

	call contr(s1nest(1:nx,1:ny),nx,ny,cint,rtime,		&
      	  'Nested grid: original',colors,pltzero,		&
      	  0,0,0,0,name)
!
! TEST SETTING NEST BCs
! set just boundary conditions to crazy value so we can see they changed
!
	flag = 10
	call dointerp(s1BCtest,s1nest,nx,ny,bcwidth, &
      	  nestX1,nestX2,nestY1,nestY2,n,ratio,flag,0,0,0,0)

	call contr(s1nest(1:nx,1:ny),nx,ny,cint,rtime,		&
      	  'Nest BCs set to -500',colors,pltzero,		&
      	  0,0,0,0,name)

!       restore normal nest BC values

	flag = 10
	call dointerp(s1coarse,s1nest,nx,ny,bcwidth, &
      	  nestX1,nestX2,nestY1,nestY2,n,ratio,flag,0,0,0,0)

	call contr(s1nest(1:nx,1:ny),nx,ny,cint,rtime,		&
      	  'Nest BCs restored',colors,pltzero,			&
      	  0,0,0,0,name)
!
! MOVE the nest to the right by half the nest size; plot both grids.
! Old & new nest locations (in coarse grid coordinates) passed to dointerp.
!
	nestX1old = nestX1
	nestX2old = nestX2
	nestY1old = nestY1
	nestY2old = nestY2

	nestX1 = nestX1 + nestsizeX/2
	nestX2 = nestX1 + nestsizeX

	flag = -1
	call dointerp(s1coarse,s1nest,nx,ny,bcwidth,	&
      	  nestX1   ,nestX2   ,nestY1   ,nestY2   ,n,ratio,flag,	&
      	  nestX1old,nestX2old,nestY1old,nestY2old)

	call contr(s1coarse(1:nx,1:ny),nx,ny,cint,rtime,		&
      	  'Coarse grid w/moved nest',colors,pltzero,			&
      	  nestX1,nestX2,nestY1,nestY2,name)
	call contr(s1nest(1:nx,1:ny),nx,ny,cint,rtime,			&
      	  'Nested grid: after move',colors,pltzero,			&
      	  0,0,0,0,name)
!
! Test FEEDBACK by greatly changing the coarse grid values, and then
! interpolating the nest back to the coarse grid; plot the coarse grid.
! 'nestX1old' and such (last four arguments to dointerp) are ignored.
!
	do j = 1,ny
	  do i = 1,nx
	    s1coarse(i,j) = -1.
	  enddo
	enddo

	flag = 2
	call dointerp(s1coarse,s1nest,nx,ny,bcwidth,			&
      	  nestX1   ,nestX2   ,nestY1   ,nestY2   ,n,ratio,flag,		&
      	  0,0,0,0)

	call contr(s1coarse(1:nx,1:ny),nx,ny,cint,rtime,		&
      	  'Coarse grid after feedback',colors,pltzero,			&
      	  nestX1,nestX2,nestY1,nestY2,name)
!
! . . . Done
!
	call clsgks
	stop
	end
!
! Spring 2016
