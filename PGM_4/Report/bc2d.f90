!=====================bc2d================================!
! inputs
! q1            Array dim (nx,ny)
! q1new         Array dim w/ gpoints
! nx
! ny            
! gpoints       number of ghost points
!
!
!=========================================================!


subroutine bc2d(q1, q1new,nx,ny,gpoints)

implicit none

integer, intent(in)                                               :: nx,ny,gpoints
real,dimension(1:nx,1:ny), intent(in)                                    :: q1
real, dimension(1-gpoints:nx+gpoints, 1-gpoints:ny+gpoints), intent(out) :: q1new

integer         :: i,j

! Loop to add bcs
q1new(1:nx, 1:ny) = q1

! Set y ghost gpoints
do i =1,nx
    do j=1-gpoints,0
      q1new(i,j) = q1(i,1)
      q1new(i, ny+1-j) = q1(i,ny)
   end do
end do

! Set X ghost points
do j =1,ny
    do i=1-gpoints,0
      q1new(i,j) = q1(1,j)
      q1new(nx+1-i, j) = q1(nx,j)
   end do
end do
return
end subroutine bc2d
