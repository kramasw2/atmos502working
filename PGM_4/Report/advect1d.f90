
! ======================= advect1d  ===================
! Integrate forward (advection only) by one time step.
! ATMS 502 / CSE 566, Spring 2016
!
! Arguments:
!
!   q_1d	   input    real array   values at current time step
!   q2_1d          output   real array   values at next time step
!   vel_1d         input    real array   velocity values at current ts (u/v)
!   dx             input    real         grid spacing
!   dt             input    real         time step
!   nx             input    integer	 number of grid points
!   advection_type input    character    'L' if Lax Wendroff, 'U' if Upstream
!   gpoints        input    integer      # of ghostpoints
!   aflag          input    integer      1 if coarse grid; 2 if nested
! 
  subroutine advect1d(q_1d,q2_1d,vel_1d,dt,dx,dy,nx,ny,advection_type,gpoints,aflag)
  implicit none

  integer, intent(in)                  :: nx,ny, gpoints
  real, intent(in)                     :: dx,dy,dt
  real, dimension(1-gpoints:nx+gpoints), intent(inout)  :: q_1d
  real, dimension(1-gpoints:nx+gpoints), intent(out) :: q2_1d
  character, intent(in)                :: advection_type
  real, dimension(1:nx+1), intent(in)              :: vel_1d
  integer :: i, aflag
  real    :: courant,c


! Lax Wendroff 1D advection ------------------------------------

! Do Coarse grid stuff..........................................
if (aflag.eq.1) then
  if (advection_type == 'L') then 
    do i = 1,nx
      c = 0.5*(vel_1d(i) + vel_1d(i+1)) ! Averaging vels at current and next pt
      courant = c*dt/dx
      q2_1d(i) = q_1d(i) - courant*0.5*(q_1d(i+1) - q_1d(i-1)) + (courant**2)*0.5*(q_1d(i+1) - 2*q_1d(i) + q_1d(i-1))
    end do

  else
    print*,' >Advection: unknown advection type ',advection_type
    stop
  endif

! DO nest stuff
else if (aflag.eq.2) then
  if (advection_type == 'L') then 
    do i = 2,nx-1
      c = 0.5*(vel_1d(i) + vel_1d(i+1)) ! Averaging vels at current and next pt
      courant = c*dt/dx
      q2_1d(i) = q_1d(i) - courant*0.5*(q_1d(i+1) - q_1d(i-1)) + (courant**2)*0.5*(q_1d(i+1) - 2*q_1d(i) + q_1d(i-1))
    end do

  else
    print*,' >Advection: unknown advection type ',advection_type
    stop
  endif
!! 6-th order Crowley Advection ----------------------------------------
! else if (advection_type == 'C') then
! do i = 1,nx
! c = 0.5*(vel_1d(i) + vel_1d(i+1)) ! Averaging vels at current and next pt
! courant = c*dt/dx
!q2_1d(i) = q_1d(i) +  (courant)*(q_1d(i-3)-9*q_1d(i-2)+45*q_1d(i-1)-45*q_1d(i+1) + 9*q_1d(i+2) -q_1d(i+3))/60 + &
!(courant**2)*(2*q_1d(i-3) - 27*q_1d(i-2) + 270*q_1d(i-1)-490*q_1d(i)+270*q_1d(i+1)-27*q_1d(i+2) + 2*q_1d(i+3))/360 + & 
!(courant**3)*(-q_1d(i-3) +8*q_1d(i-2)-13*q_1d(i-1) +13*q_1d(i+1) -8*q_1d(i+2) + q_1d(i+3))/48 + &
!(courant**4)*(-q_1d(i-3) +12*q_1d(i-2) -39*q_1d(i-1) + 56*q_1d(i) -39*q_1d(i+1) + 12*q_1d(i+2) + -q_1d(i+3))/144 + &
!(courant**5)*(q_1d(i-3) -4*q_1d(i-2) +5*q_1d(i-1) +  -5*q_1d(i+1) + 4*q_1d(i+2) - q_1d(i+3))/240 + &
!(courant**6)*(q_1d(i-3) -6*q_1d(i-2) +15*q_1d(i-1) -20*q_1d(i) +15*q_1d(i+1) -6*q_1d(i+2) + q_1d(i+3))/720    
! end do

!! Takacs (1985) Scheme ----------------------------------------
!  else if (advection_type == 'T') then     
!     do i = 1,nx
!      c = 0.5*(vel_1d(i) + vel_1d(i+1)) ! Averaging vels at current and next pt
!      courant = c*dt/dx
!      if (courant.ge.0) then
!        q2_1d(i) = q_1d(i) - courant*0.5*(q_1d(i+1) - q_1d(i-1)) + (courant**2)*0.5*(q_1d(i+1) - 2*q_1d(i) + q_1d(i-1))- &
!              (1+courant)*(courant)*(courant-1)*(q_1d(i+1) - 3*q_1d(i) + 3*q_1d(i-1) - q_1d(i-2))/6.0
!      elseif (courant.lt.0) then
!        q2_1d(i) = q_1d(i) - courant*0.5*(q_1d(i+1) - q_1d(i-1)) + (courant**2)*0.5*(q_1d(i+1) - 2*q_1d(i) + q_1d(i-1))- &
!                  (1+abs(courant))*(courant)*(courant+1)*(q_1d(i-1) - 3*q_1d(i) + 3*q_1d(i+1) - q_1d(i+2))/6.0
!      endif      
!end do

endif

 return
  end subroutine advect1d
