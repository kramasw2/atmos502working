!
! ============================= nestwind ===========================
!
! ATMS 502/CSE 566   Brian Jewett, Univ. IL   Spring 2016
! Set the nested grid wind fields
!
! Arguments:
!	unest	real array	U field on nest, dimension (nx+1,ny), no ghost pts
!	vnest	real array	V field on nest, dimension (nx,ny+1), no ghost pts
!	nx,ny	integers	Used for u,v dimensions
!	nestX1,	integers	Bottom left corner of nest,
!	 nestY1			 in coarse grid coordinates
!	dx	real		COARSE grid spacing
!	ratio	integer		Nest refinement factor
!	icase	integer		Flow: 1=rotation, 2=deformation
!
	subroutine nestwind(unest,vnest,nx,ny,nestX1,nestY1,dx,ratio,icase)
	implicit none
	integer 		 :: nx,ny,nestX1,nestY1,ratio,icase
	real    		 :: dx
	real, dimension(nx+1,ny) :: unest
	real, dimension(nx,ny+1) :: vnest
!
	integer i,j
	real x0,y0,dxnest,x,y,k,pi
!
! ... Check for invalid arguments
!
	if (nx.lt.10.or.nx.gt.250.or.ny.lt.10.or.ny.gt.250) then
	  print*,'ERROR: nestwind: bad grid dimensions; nx,ny=',nx,ny
	  stop
	endif
	if (nestX1.lt.1.or.nestX1.gt.nx) then
	  print*,'ERROR: nestwind: invalid nestX1:',nestX1
	  stop
	endif
	if (nestY1.lt.1.or.nestY1.gt.ny) then
	  print*,'ERROR: nestwind: invalid nestY1:',nestY1
	  stop
	endif
	if (dx.le.0.0.or.dx.gt.1000.) then
	  print*,'ERROR: nestwind: invalid grid spacing dx=',dx
	  stop
	endif
	if (ratio.lt.2.or.ratio.gt.50) then
	  print*,'ERROR: nestwind: invalid nest ratio',ratio
	  stop
	endif
	if (mod(nx-1,ratio).ne.0 .or. mod(ny-1,ratio).ne.0) then
	  print*,'========================================================================='
	  print*,'ERROR: nestwind: (nx-1) or (ny-1) not even multiples of the nesting ratio'
	  print*,'nx,ny,ratio =',nx,ny,ratio
	  print*,'========================================================================='
	  stop
	endif
!
! ... coordinates of bottom left corner of nest
!
	dxnest = dx/real(ratio)
	x0 = -0.5 + dx*real(nestX1-1)
	y0 = -0.5 + dx*real(nestY1-1)
!
! ... case 1: rotational flow
!
	if (icase.eq.1) then

	  do j = 1,ny
	    y = y0 + dxnest*real(j-1)
	    do i = 1,nx+1
	      x = x0 + dxnest*real(i-1) - 0.5*dxnest
	      unest(i,j) = -2.*y
	    enddo
	  enddo

	  do j = 1,ny+1
	    y = y0 + dxnest*real(j-1) - 0.5*dxnest
	    do i = 1,nx
	      x = x0 + dxnest*real(i-1)
	      vnest(i,j) = 2.*x
	    enddo
	  enddo
!
! ... case 2: deformational flow
!
	else if (icase.eq.2) then

	  pi = 4.*atan(1.)
	  k  = 6.0*pi

	  do j = 1,ny
	    y = y0 + dxnest*real(j-1)
	    do i = 1,nx+1
	      x = x0 + dxnest*real(i-1) - 0.5*dxnest
	      unest(i,j) = sin(k*(x+.5)) * sin(k*(y+.5))
	    enddo
	  enddo

	  do j = 1,ny+1
	    y = y0 + dxnest*real(j-1) - 0.5*dxnest
	    do i = 1,nx
	      x = x0 + dxnest*real(i-1)
	      vnest(i,j) = cos(k*(x+.5)) * cos(k*(y+.5))
	    enddo
	  enddo
!
! ... check for invalid icase value
!
	else
	  print*,'nestwind: invalid icase=',icase
	  stop
	endif

	return
	end
