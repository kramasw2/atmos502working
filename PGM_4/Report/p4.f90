!
!  ATMS 502 / CSE 566 -- Spring, 2016
!  Program 4:  Linear and nonlinear advection
!  >>>>> Karthik Ramaswamy <<<<<
!
  program pgm4
  implicit none

! ... Definitions ............................................................
! "nx" is the number of physical grid points in x - Not including 'ghost points'
! "ny" is the number of physical grid points in y - Not including 'ghost points' 
! "maxstep" is the maximum number of time steps.
! "c" is [constant] flow speed (e.g. meters/sec) - used only in linear case
! "dx" is grid spacing: distance (e.g. meters) between grid points in X, Y
! "name" is a variable containing your name - used to label the plots.
!"qt" is the true solution (initial condition)
! "qd" is the approximated solution (either L.W or Upstream)
! "gpoints" is the number of ghostpoints passed to advection
! "rr" is the refinement ratio
! "q1new" is the q field with added ghost points
! "q1nest" is the q field on the nested grid (w/ghost points)
! "aflag" is a flag passed to the advectio routine
! "qmax_t" is an array to store the max q at each point from every ts
! "

 
  integer, parameter      :: nx=100
  integer, parameter      :: ny=100
  integer, parameter      :: maxstep=1000
  integer, parameter      :: gpoints = 2
  real, parameter         :: c=1.0
  real, parameter         :: dx=1.0/real(nx-1)
  real, parameter         :: dy=1.0/real(ny-1)
  character*30, parameter :: name="Karthik Ramaswamy"

! ... Other variables

  real    :: dt,courant,qmax, qmin, rtime, etot, ediss, edisp, rho, pi,angh, angv, r
  integer      :: i,j,n,nplot, nstep, rr, nestX1, nestX1old, nestX2, nestX2old,& 
nestY1, nestY1old,nestY2,nestY2old,aflag,tn, feed, nmove
  character*1  :: reply,advection_type, label

! ... Arrays

  real, dimension(1:nx,1:ny)         :: q1,qt,qd, terr, qold, nest, qmax_t
  real, dimension(1-gpoints:nx+gpoints,1-gpoints:ny+gpoints)    ::q1long,q1nest
  real, dimension(nx+1,ny)           :: u, unest
  real, dimension(nx,ny+1)           :: v, vnest
  real, dimension(maxstep)    :: strace(maxstep) 
  real, dimension(1:nx, 1:ny) :: q_plot
  real,dimension(1:maxstep)     :: qmin_ar, qmax_ar
  real, dimension(nx,ny,0:maxstep) :: history

! ... Start, print, input ...........................................

  print*,'Program #4    Numerical Fluid Dynamics, Spring 2016'
  print*,' '

  print*,'Enter  Time step size:'
  read*, dt
  print*,'Enter cone radius:'
  read*, r
  print*,'Enter Desired refinement ratio:'
  read*, rr
  print*,'Enter Nest Movement interval:'
  read*, nmove
  print*,'Would you like feedback on? Enter 0: OFF; 1:ON'
  read*, feed
  print*,'Enter number of time steps to take:'
  read*,nstep

  courant = c*dt/dx

 write(6,1) dt, courant, nint( dx*real(nx) / c / dt )
  
1 format(' For time step dt = ',f5.3,', courant number = ',f6.3,/, &
         ' Number of time steps for a complete loop  = ',i3)

  
  print*,'Enter plot interval, in steps (1=each step):'
  read*,nplot

  print*,'Enter L for Linear  Lax-Wendroff, C for 6th Order Crowley, T for Takacs  advection:'
  read*,advection_type

  
! ... Open the NCAR Graphics package and set colors.
! ... To invert black/white colors, comment out calls to gscr below.

   call opngks
   call gscr(1,0,1.,1.,1.)
   call gscr(1,1,0.,0.,0.)


! ... Set and plot the initial condition..................................
history = 0.0

call ic(q1,dx,dy,nx,ny, u,v,q_plot,r)
qt = q1
! Initialize the nest - - - - - - - - - - - - - - - - - - - -
call nestfind(nestX1,nestX2,nestY1,nestY2,q1,u,v,dx,dt,nx,ny,gpoints,terr,rr)

call bc2d(q1, q1long,nx,ny,gpoints)
call dointerp(q1long,q1nest,nx,ny,gpoints,        &
             nestX1,nestX2,nestY1,nestY2,1,rr,1,  &
             0,0,0,0)

call  nestwind(unest,vnest,nx,ny,nestX1,nestY1,dx,rr,1)

! Initial condition plots .................................................
!call contr(q1,nx,ny,0.5, rtime,'Initial Condition',0,.true.,nestX1,nestX2,&
!nestY1,nestY2,name)

nest = q1nest(1:nx,1:ny)

!call contr(nest,nx,ny,0.5, rtime,'Initial Condition-nest',0,.false.,0,0,&
!0,0,name)

!call contr(u(1:nx,1:ny),nx,ny,0.1, rtime,'Initial Condition - U field',0,.true.,nestX1,nestX2,&
!nestY1,nestY2,name)

!call contr(v(1:nx,1:ny),nx,ny,0.1, rtime,'Initial Condition - V field',0,.true.,nestX1,nestX2,&
!nestY1,nestY2,name)

!call contr(terr,nx,ny,0.5, rtime,'Initial Condition',0,.false.,nestX1,nestX2,&
!nestY1,nestY2,name)

!call sfc(q1,nx,ny,rtime,angh,angv,label,name)
!.............................................................................

angh = -45.0
angv =  20.0

!! ... Integrate .....................................................
!.....................................................................
   do n = 1,nstep
!! ADVECT COARSE GRID
   call  advection(q1,u,v,nx,ny,dx,dy,dt,advection_type, gpoints,1)

! SET NEST BCs: q1, u,v   
   call bc2d(q1, q1long,nx,ny,gpoints)
   call dointerp(q1long,q1nest,nx,ny,gpoints,        &
               nestX1,nestX2,nestY1,nestY2,n,rr,10,  &
               0,0,0,0)
   call  nestwind(unest,vnest,nx,ny,nestX1,nestY1,dx,rr,1)

! ADVECT NEST FOR 3 TIMESTEPS
 nest = q1nest(1:nx,1:ny)
   do tn = 1,rr
   call  advection(nest,unest,vnest,nx,ny,dx,dy,dt,advection_type, gpoints,2)
   end do
q1nest(1:nx,1:ny) = nest

! STORE OLD q1 COARSE IN qold
   qold = q1long(1:nx,1:ny)

! FEEDBACK NEST TO COARSE GRID
if (feed.eq.1) then
call dointerp(q1long,q1nest,nx,ny,gpoints,         &
               nestX1,nestX2,nestY1,nestY2,n,rr,2,    &
               0,0,0,0)
endif

! COPY UPDATED q1 COARSE GRID
   q1 = q1long(1:nx,1:ny)

! MOVE NEST BASED ON NMOVE 
  if (mod(n,nmove).eq.0) then 
        ! STORE OLD NEST POSITION
        nestX1old = nestX1
        nestX2old = nestX2
        nestY1old = nestY1
        nestY2old = nestY2
        !RELOCATE NEST POSITION
        call nestfind(nestX1,nestX2,nestY1,nestY2,q1,u,v,dx,dt,nx,ny,gpoints,terr,rr)
        ! DOINTERP MOVE NEST WITH QOLD
        call bc2d(qold, q1long,nx,ny,gpoints)
        call dointerp(q1long,q1nest,nx,ny,gpoints,        &
             nestX1,nestX2,nestY1,nestY2,1,rr,-1,         &
             nestX1old,nestX2old,nestY1old,nestY2old)
 end if

!! CALL CONTR TO PLOT CURRENT TIME STEP CONTOURS
       rtime = dt*real(n) 
       if (mod(n,nplot).eq.0) then
!           call contr(q1,nx,ny,0.5, rtime,'Q field at time T',0,.false.,nestX1,nestX2,nestY1,nestY2,name)
           call sfc(q1,nx,ny,rtime,angh,angv,label,name)
       endif

!! PLOT FINAL RESULTS
    if (n .eq. nstep) then
!           call contr(q1,nx,ny,0.5, rtime,'FINAL SOLUTION AT TIME t',0,.false.,nestX1,nestX2,nestY1,nestY2,name)
!           call sfc(q1,nx,ny,rtime,angh,angv,label,name)
       endif
!! !   Stats - to extract qmax and qmin
history(1:nx,1:ny,n) = q1

call stats(q1,nx,ny,n,qmax, qmin)
  qmin_ar(n) = qmin
  qmax_ar(n) = qmax

!! !   Check if problem out of bounds i.e. failing.
    if (qmax.gt.35) then
       print*,'Stop - solution blowing up at step',n
       nstep = n
       exit
    endif
!
enddo

!....................................................................................
!....................................................................................

! ! ... Run complete - do final plots .............................................

! FIND MAX Q AT POINT, OVER all t
qmax_t = 0.0
do i =1,nx
  do j = 1,ny
  do n = 1,nstep
  if (history(i,j,n).gt.qmax_t(i,j)) then
  qmax_t(i,j) = history(i,j,n)
  endif
end do
end do
end do

!call contr(qmax_t,nx,ny,0.5, rtime,'qmax location over t',0,.false.,0,0,0,0,name)
!call sfc(qmax_t,nx,ny,rtime,angh,angv,label,name)

! ! Error analysis Stuff -----------------------------------------------------
! Stores the final solution in qd to pass to the takacs erroranalysis function, erroral
 
call  erroral(q1,qt,nx, ny, etot, ediss, edisp, rho)
print*,'Total Error = ',etot
print*,'Dissipation Error =', ediss
print*,'Dispersion Error=', edisp
print*,'Max correlation=', rho


! ! ... Plot Qmax(t), Qmin(t), etc.

!call ezy(qmin_ar(1:nstep),n,'Min Q. vs. t')
!call ezy(qmax_ar(1:nstep),n,'Max Q. vs. t')

print*,'Plotting surface.'
!!write(label,'(''Surface plot at n = '',i2)') n

! ! ... Close graphics package and stop.

  call clsgks
  stop
  end program pgm4
