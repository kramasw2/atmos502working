
! ============================nestfind=====================
! Finds the nest boundaries based on truncation error computation
! ATMS 502 / CSE 566, Spring 2016
!
! Arguments:
!
!   q1  output  real array   holds initial condition
!   dx  input   real         grid spacing
!   nx  integer              number of grid points

  subroutine nestfind(nestX1,nestX2,nestY1,nestY2,q1,u,v,dx,dt,nx,ny,gpoints,terr,rr)
  implicit none

  integer, intent(inout)                      :: nx,ny
  integer, intent(out)                        :: nestX1,nestX2,nestY1, nestY2
  integer, intent(in)                         :: rr
  real, intent(in)                             :: dx,dt,gpoints
  real, dimension(1:nx, 1:ny),intent(inout)      :: terr
  real, dimension(1:nx, 1:ny), intent(in)     :: q1
  real, dimension(-1:nx+2, -1:ny+2)     :: q1new
  real, dimension(1:nx, 1:ny)                  :: terrX, terrY
  real, dimension(1:nx+1, 1:ny), intent(in)     :: u
  real, dimension(1:nx, 1:ny+1), intent(in)     :: v
  real                                         :: x,y
  integer :: i,j, counter,centerx,centery,nestsize
  real    :: c,qxxx, qyyy, courant, temax
   
  terr = 0.0

! Loop to add bcs
q1new(1:nx, 1:ny) = q1

! Set y gpoints
do i =1,nx
    do j=1-gpoints,0
      q1new(i,j) = q1(i,1)
      q1new(i, ny+1-j) = q1(i,ny)
   end do
end do

! Set X  gpoints
do j =1,ny
    do i=1-gpoints,0
      q1new(i,j) = q1(1,j)
      q1new(nx+1-i, j) = q1(nx,j)
   end do
end do


! Loop Generating X truncation error
  do i = 1,ny
       do j = 1,nx
       c = 0.5*(u(i,j) + u(i+1,j)) ! Averaging vels at current and next pt
       courant = c*dt/dx
       qxxx = (q1new(i+2,j) - 2*q1new(i+1,j) + 2*q1new(i-1,j) -q1new(i-2,j))/(2*dx**3)
       terrX(i,j) = abs(qxxx*(courant**2 - 1)*(c*(dx**2)/6))
       enddo
  enddo

! Loop Generating Y truncation error
  do j = 1,nx
       do i = 1,ny
       c = 0.5*(v(i,j) + v(i,j+1)) ! Averaging vels at current and next pt
       courant = c*dt/dx
       qyyy = (q1new(i,j+2) - 2*q1new(i,j+1) + 2*q1new(i,j-1) -q1new(i,j-2))/(2*dx**3)
       terrY(i,j) = abs(qyyy*(courant**2 - 1)*(c*(dx**2)/6))
       enddo
  enddo

terr = terrX
! Choose max of x or y truncation errors
 do i = 1,nx
        do j = 1,ny
           if (terrY(i,j).gt.terr(i,j)) then
           terr(i,j) = terrY(i,j)
           end if
        end do 
 end do

!Find absolute max truncation error
temax = maxval(terr)

counter = 0
do i = 1,nx
   do j = 1,ny
       if (terr(i,j).ge.(0.5*temax)) then
          if (counter==0) then
            nestX1 = i
            counter = 1      
          else
            nestX2 = i
          endif
        endif
   end do
end do 


counter = 0
do j = 1,ny
   do i = 1,nx
       if (terr(i,j).ge.(0.5*temax)) then
          if (counter==0) then
            nestY1 = j
            counter = 1      
          else
            nestY2 = j
          endif
        endif
   end do
end do 

 centerx = (nestX1 + nestX2)/2
 centery = (nestY1 + nestY2)/2
!Check if nestsize is 3
nestsize=(nx-1)/rr
nestX1 = centerx - nestsize/2
nestX2 = nestX1 + nestsize 
!
nestY1 = centery - nestsize/2
nestY2 = nestY1 + nestsize

! Check if nest is moving out of coarse domain bounds
if (nestX1.le.1) then
  nestX1 = 1
  nestX2 = nestX1 + nestsize
else if (nestX2.ge.nx) then
   nestX2 = nx
   nestX1 = nestX2 - nestsize
endif

if (nestY1.le.1) then
  nestY1 = 1
  nestY2 = nestY1 + nestsize
else if (nestY2.ge.ny) then
  nestY2 = ny
  nestY1 = nestY2 - nestsize
endif


print*,nestX1, nestX2
print*, nestY1, nestY2
print*,maxloc(terr) 
print*, temax
return
  end subroutine nestfind

