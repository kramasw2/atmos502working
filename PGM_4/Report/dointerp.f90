!
! ....................... Grid interpolation routine ........................
!
! Routine dointerp - interpolation between coarse, nested grids
! ATMS 502/CSE 566   Brian Jewett, University of Illinois   Spring 2016
!
! Arguments:
!	coarse	  real		in/out	coarse data array; NOTE: is NOT (nx,ny)
!	nest	  real		in/out	nest data array    NOTE: is NOT (nx,ny)
!					>> "coarse" , "nest" include ghost pts!
!	nx,ny     integer	in	interior dimensions of coarse, nest arrays
!       bcwidth   integer       in      number of ghost points in each dimension
!	nestX1,	  integers	in	nest start, end indices in X and Y
!	  nestX2,			  ... for I
!	  nestY1,
!	  nestY2			  ... for J
!	nstep	 integer	in	time step counter (for print messages)
!	ratio	 integer	in	nest refinement (time, space) ratio
!	flag	 integer	in	interpolation flag:
!					  There are four choices >>>
!					 -1=interp: coarse => nest, using
!					    old grid info where possible
!					    This is for a nest move.
!					  1=interp: coarse => nest
!					    This is for nest initialization.
!					 10=interp: coarse => nest BCs
!					    This sets nest BCs (boundary points)
!					    only; nest interior is unchanged.
!					  2=interp: nest => coarse
!					    This is for feedback.
!	nestX1old, integers	in	If flag < 0, these are the OLD
!	  nestX2old,			  nested grid indices (coordinates
!	  nestY1old,			  on coarse grid) for interpolation.
!	  nestY1/2old  integers	in	  These are *ignored* if flag > 0.
!
! Internal:
!	ver	logical		local	if set true below, verbose
!					(extra print statements done)
!

    subroutine dointerp(coarse,nest,nx,ny,bcwidth,                      &
                        nestX1,nestX2,nestY1,nestY2,nstep,ratio,flag,   &
                        nestX1old,nestX2old,nestY1old,nestY2old)
    implicit none

! . Arguments

    integer :: nx,ny,bcwidth,nestX1,nestX2,nestY1,nestY2,               &
               nstep,ratio,flag,nestX1old,nestX2old,                    &
               nestY1old,nestY2old
    real, dimension(1-bcwidth:nx+bcwidth,1-bcwidth:ny+bcwidth) :: coarse,nest

! . Local variables

    integer :: icoarse,jcoarse,iNew1,iNew2,iOld1,iOld2,     &
               jNew1,jNew2,jOld1,jOld2,iold,jold,inew,jnew, &
               inest,jnest,ncopy,istep

! . Array to store old nest data for nested grid moves
    real, dimension(:,:), allocatable :: olddata

! . Variables for bilinear interpolation to nest.
    real :: ifraction,jfraction,offset,qBL,qBR,qTL,qTR,tmp1,tmp2
    logical,parameter :: ver = .true.

! . If verbose option set, write out info now.

    if (ver) then
      if (flag.eq.1) then
        write(6,10) '> coarse to nest',nstep,nestX1,nestY1,ratio
      else if (flag.lt.0) then
        write(6,10) '> coarse to nest w/nest copy',nstep,nestX1,nestY1,ratio
      else if (flag == 10) then
        write(6,10) '> setting nest BCs',nstep,nestX1,nestY1,ratio
      else
        write(6,10) '> nest to coarse',nstep,nestX1,nestY1,ratio
10        format(' Interpolating ',a,'; step ',i3,', nestX1,Y1 ',2i4,', ratio ',i1)
      endif
    endif

! . Check for invalid or unlikely arguments

    if (nx.lt.10.or.nx.gt.1000.or.ny.lt.10.or.ny.gt.1000) then
      print*,'ERROR: dointerp: bad nx or ny arguments?',nx,ny
      stop
    endif
    if (bcwidth.lt.0.or.bcwidth.gt.4) then
      print*,'ERROR: dointer: bad bcwidth?',bcwidth
      stop
    endif
    if (nestX1.lt.1.or.nestX1.gt.nx .or.        &
        nestX2.lt.1.or.nestX2.gt.nx .or.        &
        nestY1.lt.1.or.nestY1.gt.ny .or.        &
        nestY2.lt.1.or.nestY2.gt.ny) then
      write(6,15) nestX1,nestX2,nestY1,nestY2
15      format('ERROR: dointerp: one or more invalid nest arguments:',/,     &
              'nestX1,X2,Y1,Y2 = ',4i5,' ... Stopping.')
      stop
    endif
    if (flag.lt.0) then
      if (nestX1old.lt.1.or.nestX1old.gt.nx .or.    &
          nestX2old.lt.1.or.nestX2old.gt.nx .or.    &
          nestY1old.lt.1.or.nestY1old.gt.ny .or.    &
          nestY2old.lt.1.or.nestY2old.gt.ny) then
          write(6,16) nestX1old,nestX2old,nestY1old,nestY2old
16        format('ERROR: dointerp: one or more invalid nest arguments:',/,     &
                'nestX1old,X2old,Y1old,Y2old = ',4i5,' ... Stopping.')
        stop
      endif
    endif
    if (ratio.lt.2.or.ratio.gt.50) then
      print*,'ERROR: dointerp: bad nest refinement ratio:',ratio
      stop
    endif
    if (mod(nx-1,ratio).ne.0 .or. mod(ny-1,ratio).ne.0) then
      print*,'==========================================================================='
      print*,'WARNING: dointerp: (nx-1) or (ny-1) not even multiples of the nesting ratio'
      print*,'nx,ny,ratio =',nx,ny,ratio
      print*,'==========================================================================='
    endif

! ... FLAG < 0 or FLAG = 1 or 10:
! ... Bilinear interpolation from coarse grid to nested grid
! ... If flag<0, this is a grid move:  use old nested grid data
! ... If flag=10, we set Only the nest boundary (border, here) points.

    if (flag.lt.0.or.flag.eq.1.or.flag.eq.10) then

! . . If have old nest data, copy it to temporary space

      if (flag.lt.0) then
        allocate(olddata(nx,ny))
        olddata(1:nx,1:ny) = nest(1:nx,1:ny)
      endif

! ... Interpolate coarse grid to nested grid.
! ... nest() array is overwritten.

! . . Interior:

      do jnest = 1,ny-1
        jcoarse   = (jnest-1)/ratio + nestY1
        jfraction = real(mod(jnest-1,ratio))/real(ratio)
        if (flag == 10) then
          if (jnest == 1) then
            istep = 1
          else
            istep = 9999
          endif
        else
          istep = 1
        endif
        do inest = 1,nx-1,istep
          icoarse   = (inest-1)/ratio + nestX1
          ifraction = real(mod(inest-1,ratio))/real(ratio)
          qBL  = coarse(icoarse  ,jcoarse  )
          qBR  = coarse(icoarse+1,jcoarse  )
          qTL  = coarse(icoarse  ,jcoarse+1)
          qTR  = coarse(icoarse+1,jcoarse+1)
          tmp1 = qBL + (qBR-qBL)*ifraction
          tmp2 = qTL + (qTR-qTL)*ifraction
          nest(inest,jnest) = tmp1 + (tmp2-tmp1)*jfraction
        enddo
      enddo

! . . Top edge:

      do inest = 1,nx-1
        icoarse   = (inest-1)/ratio + nestX1
        ifraction = real(mod(inest-1,ratio))/real(ratio)
        nest(inest,ny) = coarse(icoarse,nestY2) +     &
          ifraction*( (coarse(icoarse+1,nestY2)-coarse(icoarse,nestY2)) )
      enddo

! . . Right edge:

      do jnest = 1,ny-1
        jcoarse   = (jnest-1)/ratio + nestY1
        jfraction = real(mod(jnest-1,ratio))/real(ratio)
        nest(nx,jnest) = coarse(nestX2,jcoarse) +     &
          jfraction*( (coarse(nestX2,jcoarse+1)-coarse(nestX2,jcoarse)) )
      enddo

! . . Top corner:

      nest(nx,ny) = coarse(nestX2,nestY2)

! . . If using old nest, copy overlapping data.

      if (flag.lt.0) then

! . . . No overlap case

        if (nestX2old.lt.nestX1.or.nestX1old.gt.nestX2 .or.    &
            nestY2old.lt.nestY1.or.nestY1old.gt.nestY2) then
          write(6,20) nestX1old,nestX2old,nestY1old,nestY2old, &
                      nestX1   ,nestX2   ,nestY1   ,nestY2
20          format(' Dointerp: *no overlap* between old, new nests.',/, &
                   'Old nest location (',i3,':',i3,',',i3,':',i3,')',/, &
                   'New nest location (',i3,':',i3,',',i3,':',i3,')')
        else

! . . . . X bounds for old, new nests

          if (nestX1.gt.nestX1old) then
            iOld1 = (nestX1-nestX1old)*ratio + 1
            iOld2 = nx
            iNew1 = 1
            iNew2 = iNew1 + (iOld2-iOld1)
          else
            iNew1 = (nestX1old-nestX1)*ratio + 1
            iNew2 = nx
            iOld1 = 1
            iOld2 = iOld1 + (iNew2-iNew1)
          endif

! . . . . Y bounds for old, new nests

          if (nestY1.gt.nestY1old) then
            jOld1 = (nestY1-nestY1old)*ratio + 1
            jOld2 = ny
            jNew1 = 1
            jNew2 = jNew1 + (jOld2-jOld1)
          else
            jNew1 = (nestY1old-nestY1)*ratio + 1
            jNew2 = nx
            jOld1 = 1
            jOld2 = jOld1 + (jNew2-jNew1)
          endif

! . . . . Summarize the planned nest data copy

          if (ver) then
            ncopy = (iOld2-iOld1+1)*(jOld2-jOld1+1)
            write(6,30) iOld1,iOld2,jOld1,jOld2,    &
                        iNew1,iNew2,jNew1,jNew2,    &
                        100.*real(ncopy)/real(nx*ny)
30            format(' Dointerp: copying from old nest (',    &
                     i3,':',i3,',',i3,':',i3,') to new (',    &
                     i3,':',i3,',',i3,':',i3,'); ',f5.1,    &
                     '% overlap')
          endif

! . . . . Copy overlap data from old nest to new one.

          nest(iNew1:iNew2,jNew1:jNew2) = olddata(iOld1:iOld2,jOld1:jOld2)

! . . . . end of flag<0 case
        endif
! . . . end of flag<0 or flag=1 cases
      endif

! ... FLAG = 2:
! ... Interpolate from nested grid to coarse grid
! ... Collocated point replacement here, rather
! ... than averaging of neighboring nest values

    else if (flag.eq.2) then

      do jcoarse = (nestY1+1),(nestY2-1)
        jnest = (jcoarse-nestY1)*ratio + 1
        do icoarse = (nestX1+1),(nestX2-1)
          inest = (icoarse-nestX1)*ratio + 1
          coarse(icoarse,jcoarse) = nest(inest,jnest)
        enddo
      enddo

! . Otherwise: Error, unknown flag value.

    else
      print*,'Interpolation flag=',flag,' not recognized.'
      stop
    endif

! . If we used temporary space, deallocate it now.

    if (flag.lt.0) deallocate(olddata)

! . Done.

    return
    end
