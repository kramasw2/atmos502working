! ============================ bc =====================
! BC sets the boundary conditions
! ATMS 502 / CSE 566, Spring 2016
!
! Arguments:
!
!   q1  in/output  real array  values at current time step
!   nx	input      integer     main array size, not including
!                              extra 'ghost' zones/points

  subroutine bc(q_1d,nx, ny, gpoints)
  implicit none

  integer, intent(in)                    :: nx, ny, gpoints
  real, dimension(1-gpoints:nx+gpoints), intent(inout) :: q_1d
  integer                          :: i
! call bc() 


! COARSE GRID STUFF
do i = 1-gpoints, 0 
     q_1d(i) = q_1d(1)
     q_1d(nx+1-i) = q_1d(nx)
enddo
end subroutine bc
