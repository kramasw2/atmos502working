! ========================= stats =====================
! Stats computes and prints out the max Q values
! ATMS 502 / CSE 566, Spring 2016
!
! Arguments:
!
!   q2	  input   real array   latest array values
!   nx	  input   integer      size of data array
!   n	  input   integer      time step counter, from main program
!   qmax  output  real         holds max absolute value of q2
!
    subroutine stats(q2,nx,n,qmax)
    implicit none

    integer, intent(in)                 :: n,nx
    real, intent(out)                   :: qmax
    real, dimension(0:nx+1), intent(in) :: q2

    integer :: i

    qmax = abs(q2(1))
    do i = 2,nx
      qmax = max(qmax,abs(q2(i)))
    enddo

    write(6,1) n,qmax
1   format('Step ',i3,', Max = ',f8.5)

    return

    end subroutine stats
