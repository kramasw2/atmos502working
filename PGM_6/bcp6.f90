!
! Boundary condition routine
! Program 6, compressible nonlinear 3D flow
! ATMS 502/CSE 566, Spring 2016
!
! Set the boundary conditions.
! I use 1 ghost zone for all variables but theta, which has 2.
! Be sure to modify this routine to be consistent with your array layout.
!
        subroutine bcp6(theta,u,v,w,p,nx,ny,nz,gpoints)
        implicit none
!
!  Arguments
!
        integer nx,ny,nz
        real u(0:nx+2,0:ny+1,0:nz+1),v(0:nx+1,0:ny+2,0:nz+1),w(0:nx+1,0:ny+1,0:nz+2), &
             theta(-1:nx+2,-1:ny+2,-1:nz+2),p(0:nx+1,0:ny+1,0:nz+1)
        integer i,j,k
        integer, intent(in) :: gpoints

!
! ... BCtype=3: Symmetry boundaries for X, periodic Y
!

!$OMP     PARALLEL DO PRIVATE (i,j,k)
      do k = 1,nz
        do j = 1,ny
          u(1   ,j,k) = -u(2 ,j,k)
          u(0   ,j,k) = -u(3 ,j,k)
          u(nx+1,j,k) = -u(nx,j,k)
        enddo
        do i = 1,nx
          u(i,   0,k) = u(i,  ny,k)
          u(i,ny+1,k) = u(i, 1  ,k)
        enddo
      enddo
!$OMP     END PARALLEL DO
!$OMP     PARALLEL DO PRIVATE (i,j,k)
      do k = 1,nz
        do i = 1,nx
          v(i,ny+1,k) =  v(i  ,1 ,k)
          v(i   ,0,k) =  v(i  ,ny,k)
        enddo
        do j = 1,ny+1
          v(0   ,j,k) = v(2   ,j,k)
          v(nx+1,j,k) = v(nx-1,j,k)
        enddo
      enddo
!$OMP     END PARALLEL DO
!$OMP     PARALLEL DO PRIVATE (i,j,k)
      do k = 2,nz
        do j = 1,ny
          w(0   ,j,k) = w(2   ,j,k)
          w(nx+1,j,k) = w(nx-1,j,k)
        enddo
        do i = 1,nx
          w(i,   0,k) = w(i,  ny,k)
          w(i,ny+1,k) = w(i,  1 ,k)
        enddo
      enddo
!$OMP     END PARALLEL DO
!$OMP     PARALLEL DO PRIVATE (i,j,k)
      do k = 1,nz
        do j = 1,ny
          theta( 0  ,j,k) = theta(2   ,j,k)
          theta(-1  ,j,k) = theta(3   ,j,k)
          theta(nx+1,j,k) = theta(nx-1,j,k)
          theta(nx+2,j,k) = theta(nx-2,j,k)
        enddo
        do i = 1,nx
          theta(i,   0,k) = theta(i,ny  ,k)
          theta(i,  -1,k) = theta(i,ny-1,k)
          theta(i,ny+1,k) = theta(i,  1 ,k)
          theta(i,ny+2,k) = theta(i,  2 ,k)
        enddo
      enddo
!$OMP     END PARALLEL DO
!$OMP     PARALLEL DO PRIVATE (i,j,k)
      do k = 1,nz
        do j = 1,ny
          p(0   ,j,k) = p(2   ,j,k)
          p(nx+1,j,k) = p(nx-1,j,k)
        enddo
        do i = 1,nx
          p(i,   0,k) = p(i,ny  ,k)
          p(i,ny+1,k) = p(i,  1 ,k)
        enddo
      enddo
!$OMP     END PARALLEL DO

!
! ... 0-gradient top, bottom
!

!$OMP   PARALLEL DO PRIVATE (i,j)
    do j = 0,ny+1
      do i = 0,nx+1
        u(i,j,nz+1) = u(i,j,nz)
        u(i,j,0   ) = u(i,j,1 )
      enddo
      do i = 0,nx+1
        v(i,j,nz+1) = v(i,j,nz)
        v(i,j,0   ) = v(i,j,1 )
      enddo
      do i = 0,nx+1
        w(i,j,0   ) = 0.
        w(i,j,1   ) = 0.
        w(i,j,nz+1) = 0.
      enddo
    enddo
!$OMP   END PARALLEL DO
!$OMP   PARALLEL DO PRIVATE (i,j)
    do j = -1,ny+2
          do i = -1,nx+2
            theta(i,j,0   ) = theta(i,j,1 )
            theta(i,j,-1  ) = theta(i,j,1 )
            theta(i,j,nz+1) = theta(i,j,nz)
            theta(i,j,nz+2) = theta(i,j,nz)
      enddo
    enddo
!$OMP   END PARALLEL DO

    return
    end subroutine bcp6
