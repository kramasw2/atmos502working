
! ======================= diffusion ===================
! Integrate forward (advection only) by one time step.
! ATMS 502 / CSE 566, Spring 2016
!
! Arguments:
!
!   theta1	           input    real array   values at current time step
!   q2	           output   real array   values at next time step
!   c              input    real         flow speed for linear case
!   dx,dz          input    real         grid spacing
!   dt             input    real         time step
!   nx,nz             input    integer	 number of grid points
!   advection_type input    character    'L' if linear, 'N' if nonlinear
!   u,v            input    real array   values at current ts

  subroutine  diffusion(theta1,theta2,u1, u3,w1,w3,nx,nz,dx,dz,dt,tstep, Km, Kt,gpoints)
  implicit none

  integer, intent(in)                               :: nx,nz, gpoints
  real, intent(in)                                  :: dx,dz, dt, tstep, Km, Kt
  real, dimension(0:nx+1,0:nz+1), intent(inout)     :: theta1, theta2
  real, dimension(0:nx+2, 0:nz+1), intent(inout)         :: u1,u3
  real, dimension(0:nx+1, 0:nz+2), intent(inout)         :: w1,w3
  real, dimension((1-gpoints):(nx+gpoints))         :: q_1d, q2_1d
  real, dimension(1:nx+1)                           :: vel_1d
  integer                                           :: i,j,k

! CHANGE U2 to U1 TO AVOID CONFUSION
  ! u diffusion
call bc(theta1, u1, w1, theta1,nx, nz, gpoints)
do i = 1,nx +1
 do k = 1,nz
 u3(i,k) = u3(i,k) + tstep*(Km*((u1(i+1,k) - 2*u1(i,k)+u1(i-1,k))/(dx**2) + &
 (u1(i,k+1) - 2*u1(i,k) + u1(i,k-1))/(dz**2)))
 enddo
enddo

  ! w diffusion
do i = 1,nx
  do k = 1, nz+1
 w3(i,k) = w3(i,k) + tstep*(Km*((w1(i+1,k) - 2*w1(i,k)+w1(i-1,k))/(dx**2) + &
 (w1(i,k+1) - 2*w1(i,k) + w1(i,k-1))/(dz**2)))
  enddo
enddo
  

! theta diffusion
do i = 1,nx
  do k = 1,nz
theta2(i,k) = theta2(i,k) + dt*(Kt*((theta1(i+1,k)- 2*theta1(i,k) + &
theta1(i-1,k))/(dx**2) + (theta1(i,k+1)-2*theta1(i,k)+ theta1(i,k-1))/(dz**2)))
  enddo
enddo

return
end subroutine diffusion
