
! ======================= pgf ===================
! Incorporates Pressure Gradient Forces/Buoyancy within
! the 2D Quasi-Compressible Advection/Diff eqns

! UPDATE DESCRIPTION IF POSSIBLE

! ATMS 502 / CSE 566, Spring 2016
!
! Arguments:
!
!   q1	           input    real array   values at current time step
!   q2	           output   real array   values at next time step
!   c              input    real         flow speed for linear case
!   dx,dy          input    real         grid spacing
!   dt             input    real         time step
!   nx,ny             input    integer	 number of grid points
!   advection_type input    character    'L' if linear, 'N' if nonlinear
!   u,v            input    real array   values at current ts

  subroutine pgf(theta1,u3,w3,p1,p3,nx,nz,dx,dz,tstep, rhou, rhow, gpoints, cs)
   
  implicit none

  integer, intent(in)                               :: nx,nz, gpoints
  real, intent(in)                                  :: dx,dz, tstep
  real, dimension(0:nx+1,0:nz+2), intent(inout)     :: p1,p3
  real, dimension(0:nx+2, 0:nz+1), intent(inout)      :: u3
  real, dimension(0:nx+2, 0:nz+1), intent(in)         :: rhou
  real, dimension(0:nx+1, 0:nz+2), intent(inout)      :: w3
  real, dimension(0:nx+1, 0:nz+2), intent(inout)         :: rhow
  real, dimension(0:nx+1, 0:nz+1)                   :: theta1
  real, intent(in)                                   :: cs
  integer                                           :: i,k
 call bc(theta1, u3, w3, p1, nx, nz, gpoints)
 do i = 1,nx+1
    do k = 1,nz
    u3(i,k) = u3(i,k) - tstep*(p1(i,k)-p1(i-1,k))/(rhou(i,k)*dx) 
    enddo
enddo

do i = 1,nx
   do k = 1,nz+1
 w3(i,k) = w3(i,k) - tstep*(1/(rhow(i,k))*(p1(i,k)-p1(i,k-1))/(dz))+ & 
tstep*9.81*(theta1(i,k)+theta1(i,k-1)-600)/(2*300)
   enddo
enddo
 
! Set u,w BCs
 call bc(p1, u3, w3, p1, nx, nz, gpoints)
 rhow(:,0) = rhow(:,1)
 rhow(:,nz+1) = rhow(:,nz)
 do i = 1,nx
    do k = 1,nz
     p3(i,k) = p1(i,k) - tstep*(cs**2)*(rhou(i,k)*(u3(i+1,k) - u3(i,k))/(dx)+&
      ((rhow(i,k+1))*w3(i,k+1)-(rhow(i,k))*w3(i,k))/dz)
    enddo
enddo
call bc(p3, u3, w3, p3, nx,nz,gpoints)
  return
  end subroutine pgf
