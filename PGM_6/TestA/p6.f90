!
!  ATMS 502 / CSE 566 -- Spring, 2016
!  Program 6: 3D Quasi Compressible Flow
!  >>>>> Karthik Ramaswamy <<<<<
!
  program pgm6
  implicit none

! ... Definitions ............................................................
! "nx" is the number of physical grid points in x - Not including 'ghost points'
! "nz" is the number of physical grid points in y - Not including 'ghost points' 
! "maxstep" is the maximum number of time steps.
! "c" is [constant] flow speed (e.g. meters/sec) - used only in linear case
! "dx" is grid spacing: distance (e.g. meters) between grid points in X, Y
! "name" is a variable containing your name - used to label the plots.
!"qt" is the true solution (initial condition)
! "qd" is the approximated solution (either L.W or Upstream)
! "gpoints" is the number of ghostpoints passed to advection
! 
  integer, parameter      :: nx=33
  integer, parameter      :: ny=33
  integer, parameter      :: nz=16
  integer, parameter      :: maxstep=10000
  integer, parameter      :: gpoints = 1
  real, parameter         :: dx=500
  real, parameter         :: dy=500
  real, parameter         :: dz=500
  character*30, parameter :: name="Karthik Ramaswamy"
! ... Other variables

 real  :: dt,courant,qmax, qmin, rtime, etot, ediss, edisp, rho, pi,angh, angvi, tstep
 real, parameter  :: Kt=50.0 ,Km=30.0, uper = 0.0
 real, dimension(1:2)             :: x_0,y_0, z_0
 real, dimension(1:2,1)             :: tper,vper
 real, dimension(1:2)                  :: xrad, yrad,zrad
 real, parameter                  :: cs = 80.0

 integer      :: i,j,k,n,nplot, nstep
 character*1  :: reply,advection_type, label

! ... Arrays

! Main array dimensions are assigned including GP lengths.
  real, dimension(1:nx,1:ny,1:nz)            :: q1,qt,qd
  real, dimension(0:nx+2,0:ny+1,0:nz+1)        :: u1,u2, u3 ,rhou
  real, dimension(0:nx+1,0:ny+2,0:nz+1)        :: v1, v2, v3, rhov
  real, dimension(0:nx+1,0:ny+1,0:nz+2)        :: w1, w2, w3, rhow
  real, dimension(0:nx+1,0:ny+1,0:nz+1)        ::  p1, p2, p3
  real, dimension(-1:nx+2,-1:ny+2,-1:nz+2)        :: theta1, theta2
  real, dimension(maxstep)    :: strace(maxstep) 
  real,dimension(1:maxstep)     :: qmin_ar, qmax_ar
!  real, dimension(nx,maxstep) :: history(nx,maxstep)

! ... Start, print, input ...........................................

  tper(1,1) = -20.0
  tper(2,1) = 0.0 
  x_0(1) =  8250
  y_0(1) = 8250
  z_0(1) =  4250
  x_0(2) = 8250
  y_0(2) = 8250
  z_0(2) = 4250

  xrad(1) = 4000
  yrad(1) = 4000
  zrad(1) = 4000

  xrad(2) = 4000
  yrad(2) = 4000
  zrad(2) = 4000
  print*,'Program #6    Numerical Fluid Dynamics, Spring 2016'
  print*,'P5 Final program settings'
  print*,' '

  print*,'Enter Desired Time step:'
  read*, dt
  print*,'Enter number of time steps to take:'
  read*,nstep

  print*,'Enter L for Linear  Lax-Wendroff, C for 6th Order Crowley, T for Takacs  advection:'
  read*,advection_type

  print*,'Enter plot interval, in steps (1=each step):'
  read*, nplot
 
! ... Open the NCAR Graphics package and set colors.
! ... To invert black/white colors, comment out calls to gscr below.

   call opngks
   call gscr(1,0,1.,1.,1.)
   call gscr(1,1,0.,0.,0.)

! ! ... X-axis label

!   call anotat('I','Q',0,0,0,0)

! ... Set default Y plot bounds.
 
!  call agsetf('Y/MINIMUM.',-1.2)
!  call agsetf('Y/MAXIMUM.', 1.2)

! ... Set and plot the initial condition. Note no ghost points passed to plot1d!

!  call ic(theta1,dx,dz,nx,nz,u1,w1,p1,rhou, rhow)
  call ic(theta1(1:nx,1:ny,1:nz),dx,dy,dz,nx,ny,nz, &
  u1(1:nx+1,1:ny,1:nz),v1(1:nx,1:ny+1,1:nz),w1(1:nx,1:ny,1:nz+1),p1(1:nx,1:ny,1:nz),x_0,y_0,z_0, & 
  xrad, yrad,zrad, tper,uper,vper, rhou, rhov,rhow)

!  u2 = u1
!  w2 = w1
!  p2 = p1
theta1(1:nx, 1:ny, 1:nz) = theta1(1:nx, 1:ny, 1:nz)-300.0 
! Initial condition plots as contrs
call putfield('T1',0.0,theta1(1:nx,1:ny,1:nz),nx,ny,nz)
!  call contr(p1(1:nx,1:nz),nx,nz,2.0, rtime,'Initial Condition',0,.false.,0,0,0,0,name)
!  call contr(rhou(1:nx,1:nz),nx,nz,0.5, rtime,'Rho u',0,.false.,0,0,0,0,name)
!  call contr(rhow(1:nx,1:nz),nx,nz,0.5, rtime,'Rho w',0,.false.,0,0,0,0,name)
! qt = q_plot
!call stats(q1,nx,nz,0,qmax,qmin)

!angh = -45.0
!angv =  20.0

! Before calling integrate routine, do:

!call bc(theta1, u1, w1, p1,nx, nz, gpoints)

!!! ... Integrate .....................................................
!!  do n = 1,nstep
!  
!  u3 = u1
!  w3 = w1
!  theta2 = theta1
!  p3 = p1
!!  Boundary conditions called within each physics subroutine
!  if (n.eq.1) then
!  tstep = dt
!  else
!  tstep = 2*dt
!  endif
!
!  Boundary conditions called within each physics subroutine
!   call  advection(theta1,theta2,u1,u2,u3,w1,w2,w3,nx,nz,dx,dz,dt,tstep,advection_type, gpoints)
!  call advection(theta1,theta2,u1,u2,u3,v1,v2,v3,w1,w2,w3, &
!                nx,ny,nz,dx,dy,dz,tstep,dt,advection_type, gpoints)
!   call  diffusion(theta1,theta2,u1, u3,w1,w3,nx,nz,dx,dz,dt,tstep, Km, Kt,gpoints)
!   call  pgf(theta1,u3,w3,p1,p3,nx,nz,dx,dz,tstep, rhou, rhow, gpoints, cs)
!   if (mod(n,nplot).eq.0) then
!   rtime = dt*real(n) 
!   call contr(p3(1:nx,1:nz), nx, nz, 25.0, rtime, 'P at t', 0, .false., &
!0,0,0,0,name)
 !  call contr(theta2(1:nx,1:nz)-300, nx, nz, 1.0, rtime, 'Theta at t', 0, .false., &
!0,0,0,0,name)
!   call contr(w3(1:nx,1:nz+1), nx, nz+1,2.0, rtime, 'w at t', 0, .false., &
!0,0,0,0,name)
!   call contr(p3(1:nx,1:nz), nx, nz,25.0, rtime, 'P at t', 0, .false., &
!0,0,0,0,name)
!   call contr(u3(1:nx+1,1:nz), nx+1, nz,2.0, rtime, 'w at t', 0, .false., &
!0,0,0,0,name)
!   call contr(theta2(1:nx,1:nz) - 300.0, nx, nz,1.0, rtime, 'Theta at t', 0, .false., &
!0,0,0,0,name)
!endif
! Update values for next ste

  
!!if (n.eq.1) then
!  u2 = u3
!  w2 = w3
!  p2 = p3
!  theta1 = theta2
!else
!  u1 = u2
!  w1 = w2
!  p1 = p2
!  u2 = u3
!  p2 = p3
!  w2 = w3
!  theta1 = theta2
!endif
!print*, "STEP ----"
!print*, maxval(p3)
!print*, minval(p3)
!print*, "-------"
!print*, minval(u3)
!print*, maxval(u3)
!print*, "-------"
!print*, maxval(w3)
!print*, minval(w3)
!print*, minval(theta2)
!print*, maxval(theta2)
!if (maxval(u3).gt.100) then
!print*,"timestep blown"
!print*, n
!print*, maxloc(u3)
!EXIT
!endif

! !   Stats - to extract qmax and qmin
! call stats(q1,nx,nz,n,qmax, qmin)
!     strace(n) = qmax
! qmin_ar(n) = qmin
! qmax_ar(n) = qmax
! !   Check if problem out of bounds i.e. failing.
!    if (qmax.gt.35) then
!       print*,'Stop - solution blowing up at step',n
!      call plot1d(q2(1:nx),nx,n,qmax,.false.,qtrue(1:nx),'Solution blowing up',name)
!       nstep = n
!       exit
!    endif

! !   end of time loop from n=1 to nstep
!  enddo
!
! Stores the final solution in qd to pass to the takacs erroranalysis function, erroral

 !qd = q1
 
! call  erroral(qd,qt,nx, nz, etot, ediss, edisp, rho)
!print*,'Total Error = ',etot
!print*,'Dissipation Error =', ediss
!print*,'Dispersion Error=', edisp
!print*,'Max correlation=', rho

! ! ... Run complete - do final plots .................................

! ! ... Plot Qmax(t)


!call ezy(qmin_ar(1:nstep),n,'Min Q. vs. t')
!call ezy(qmax_ar(1:nstep),n,'Max Q. vs. t')

!print*,'Plotting surface.'
!!write(label,'(''Surface plot at n = '',i2)') n

! !  call agsetf('Y/MINIMUM.',0.)
! !  call agsetf('Y/MAXIMUM.',1.5)
! !  call anotat('N','Max value',0,0,0,0)
! !  call plot1d(strace,nstep,0,qmax,.false.,qtrue(1:nx),'Qmax vs. time',name)

! ! ... Plot history (time-space) surface s(x,t)

! !  call sfc(history,nx,nstep,dt*real(nstep),-90., 0.,    &
! !      	   'Time-Space evolution (view: -90,  0)',name)
! !  call sfc(history,nx,nstep,dt*real(nstep),-75., 5.,    &
! !      	   'Time-Space evolution (view: -75, +5)',name)
! !  call sfc(history,nx,nstep,dt*real(nstep),-30.,20.,    &
! !      	   'Time-Space evolution (view: -30,+20)',name)

! ! ... Close graphics package and stop.

  call clsgks
  stop
  end program pgm6
