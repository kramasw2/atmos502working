!
! Simple array multiply, parallel timing test program with OpenMP
! To compile for 1-cpu or parallel: type "make parallel-test"
! ATMS 502/CSE 566     Spring 2016     Jewett
!
! Want your Fortran code to run faster?  Consider these web pages:
! https://www.erdc.hpc.mil/docs/Tips/improvePerf.html
! https://docs.oracle.com/cd/E19205-01/819-5262/aeuhf/index.html
! http://www.phy.ohiou.edu/~hadizade/MRHadizadeh/Blog/Entries/2013/8/5_Tips_and_tricks_for_optimization_of_Fortran_codes.html
! http://jblevins.org/log/efficient-code    [applies to C and Fortran]
!
	program parallel_test
	implicit none

	integer, parameter       :: nx=512,ny=nx,nz=ny
	integer                  :: nthreads,threadID,omp_get_num_threads, &
                                    omp_get_thread_num,n,nstep,i,j,k
	real,dimension(nx,ny,nz) :: t1,t2
        real                     :: dt,Km,dx

	print*,'Test job starting.'

!
! ... begin parallel section -- start series of threads
! ... if threadID is 0, this is the master, which can get thread count
! ... parallelization requires $OMP comment directives in the code; the
! ... "ifdef" directives allow compilation to work withOut parallelization.
!

#ifdef _OPENMP
!$OMP   PARALLEL PRIVATE(threadID)
        threadID = OMP_GET_THREAD_NUM()
!       print*,'Thread active: ',threadID
        if (threadID.eq.0) then
          nthreads = OMP_GET_NUM_THREADS()
          write(6,'(''Test running with threads = '',i2)') nthreads
        endif
!$OMP   END PARALLEL
#else
        print*,'Test is running single-core (1 thread)'
#endif

	nstep = 200
	Km    = 50.
	dx    = 1./real(nx-1)
	dt    = 0.1*dx*dx/Km

	print*,'Taking',nstep,' steps.'

!$OMP   PARALLEL DO PRIVATE(i,j,k)
	do k = 1,nz
	  do j = 1,ny
	    do i = 1,nx
	      t1(i,j,k) = 10.*cos(200.*3.1415926*real(i-1+j+k))
	    enddo
	  enddo
	enddo
!$OMP	END PARALLEL DO

	do n = 1,nstep
	  if (mod(n,50).eq.0) print*,'Starting step',n
!
!$OMP   PARALLEL DO PRIVATE(i,j,k)
	  do k = 1,nz
	    do j = 2,ny-1
	      do i = 2,nx-1
	        t2(i,j,k) = t1(i,j,k) + (Km*dt)/(dx*dx)*    &
      		    ( t1(i+1,j,k)-2.*t1(i,j,k)+t1(i-1,j,k) )
	      enddo
	    enddo
	  enddo
!$OMP   END PARALLEL DO
!$OMP   PARALLEL DO PRIVATE(i,j,k)
	  do k = 1,nz
	    do j = 2,ny-1
	      do i = 2,nx-1
	        t1(i,j,k) = t2(i,j,k)
	      enddo
	    enddo
	  enddo
!$OMP   END PARALLEL DO
!
	enddo

	print*,'Test job stopping.'
	stop
	end
