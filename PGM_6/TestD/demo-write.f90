!
!  Demo-write    Atms 502     Spring 2016
!  Brian Jewett, Univ. Illinois Atmospheric Sciences
!
!  This program calls the putfield() routine
!
!   COMPILE this program as:  ifort -free demo-write.F putfield.F -o demo-write
!   (i.e. compile together while leaving routines in separate files.)

!
	program demo
	implicit none
!
	integer,parameter :: nx=150,ny=nx,nz=64
        real, dimension(nx,ny,nz) :: u2,v2,w2,t2,p2

	integer   :: i,j,k,step
	real      :: rad
	real,save :: dt=5.0

	do step = 1,3
	  print*,'step = ',step

	  do k = 1,nz
	    do j = 1,ny
	      do i = 1,nx
	        rad = real(step)*sqrt( real(i*i) + real(j*j) + real(k*k) )
	        u2(i,j,k) = real(nx)-rad
	        v2(i,j,k) = u2(i,j,k)
	        w2(i,j,k) = u2(i,j,k)
	        t2(i,j,k) = u2(i,j,k)+300.
	        p2(i,j,k) = u2(i,j,k)
	      enddo
	    enddo
	  enddo

! ... in your code you would interpolate all U,V,W values to T/P locations.
! ... write out (nx,ny,nz) data

	  call putfield('T',dt*real(step),t2,nx,ny,nz)
	  call putfield('U',dt*real(step),u2,nx,ny,nz)
	  call putfield('V',dt*real(step),v2,nx,ny,nz)
	  call putfield('W',dt*real(step),w2,nx,ny,nz)
	  call putfield('P',dt*real(step),p2,nx,ny,nz)

	enddo

	print*,'demo-write is done.  Your data set is in: RunHistory.dat'
	stop
	end
