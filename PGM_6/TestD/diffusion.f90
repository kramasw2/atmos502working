
! ======================= diffusion ===================
! Integrate forward (advection only) by one time step.
! ATMS 502 / CSE 566, Spring 2016
!
! Arguments:
!
!   theta1	           input    real array   values at current time step
!   q2	           output   real array   values at next time step
!   c              input    real         flow speed for linear case
!   dx,dz          input    real         grid spacing
!   dt             input    real         time step
!   nx,nz             input    integer	 number of grid points
!   advection_type input    character    'L' if linear, 'N' if nonlinear
!   u,v            input    real array   values at current ts

  subroutine  diffusion(theta1,theta2,u1, u3,v1,v3,w1,w3,nx,ny,nz,dx,dy,dz,dt,tstep, Km, Kt,gpoints)
  implicit none

  integer, intent(in)                               :: nx,ny,nz, gpoints
  real, intent(in)                                  :: dx,dy,dz, dt, tstep, Km, Kt
  real, dimension(0:nx+1,0:ny+1,0:nz+1), intent(inout)     :: theta1, theta2
  real, dimension(0:nx+2, 0:ny+1,0:nz+1), intent(inout)         :: u1,u3
  real, dimension(0:nx+1, 0:ny+2,0:nz+1), intent(inout)         :: v1,v3
  real, dimension(0:nx+1,0:ny+1, 0:nz+2), intent(inout)         :: w1,w3
  integer                                           :: i,j,k

! CHANGE U2 to U1 TO AVOID CONFUSION
  ! u diffusion
call bcp6(theta1, u1, v1, w1, theta1,nx, ny,nz, gpoints)
do i = 1,nx +1
 do k = 1,nz
   do j = 1,ny
 u3(i,j,k) = u3(i,j,k) + tstep*(Km*((u1(i+1,j,k) - 2*u1(i,j,k)+u1(i-1,j,k))/(dx**2) + &
             (u1(i,j+1,k) - 2*u1(i,j,k)+u1(i,j-1,k))/(dy**2)+ &
             (u1(i,j,k+1) - 2*u1(i,j,k) + u1(i,j,k-1))/(dz**2)))
   enddo
 enddo
enddo


! v diffusion
do i = 1,nx
  do k = 1, nz
     do j = 1,ny+1
 v3(i,j,k) = v3(i,j,k) + tstep*(Km*((v1(i+1,j,k) - 2*v1(i,j,k)+v1(i-1,j,k))/(dx**2) + &
             (v1(i,j+1,k) - 2*v1(i,j,k)+v1(i,j-1,k))/(dy**2) + &
             (v1(i,j,k+1) - 2*v1(i,j,k) + v1(i,j,k-1))/(dz**2)))
     enddo
  enddo
enddo


! w diffusion
do i = 1,nx
  do k = 1, nz+1
     do j = 1,ny
 w3(i,j,k) = w3(i,j,k) + tstep*(Km*((w1(i+1,j,k) - 2*w1(i,j,k)+w1(i-1,j,k))/(dx**2) + &
             (w1(i,j+1,k) - 2*w1(i,j,k)+w1(i,j-1,k))/(dy**2) + &
             (w1(i,j,k+1) - 2*w1(i,j,k) + w1(i,j,k-1))/(dz**2)))
     enddo
  enddo
enddo

  

! theta diffusion
do i = 1,nx
  do k = 1,nz
    do j = 1,ny
theta2(i,j,k) = theta2(i,j,k) + dt*(Kt*((theta1(i+1,j,k)- 2*theta1(i,j,k) + &
        theta1(i-1,j,k))/(dx**2) + (theta1(i,j+1,k) -2*theta1(i,j,k) + theta1(i,j-1,k))/(dy**2) + &
        (theta1(i,j,k+1)-2*theta1(i,j,k)+ theta1(i,j,k-1))/(dz**2)))
     enddo
  enddo
enddo

return
end subroutine diffusion
