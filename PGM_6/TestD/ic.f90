! ============================ ic =====================
! IC sets the initial condition
! ATMS 502 / CSE 566, Spring 2016
!
! Arguments:
!
!   theta  output  real array   holds initial condition
!   dx  input   real         grid spacing
!   nx  integer              number of grid points

  subroutine ic(theta,dx,dy,dz,nx,ny,nz,u,v,w,p,x_0,y_0,z_0,xrad,yrad,zrad, tper,uper,vper,rhou,rhov,rhow)
  implicit none

  integer, intent(in)                                  :: nx,ny,nz
  real, intent(in)                                     :: dx, dy,dz
  real, dimension(1:nx, 1:ny,1:nz), intent(out)        :: theta,p
  real, dimension(1:2,1), intent(in)                   :: tper, vper 
  real, dimension(1:nx+1, 1:ny,1:nz), intent(out)      :: u
  real, dimension(0:nx+2,0:ny+1,0:nz+1)                :: rhou 
  real, dimension(1:nx,1:ny, 1:nz+1)                   :: w
  real, dimension(0:nx+1,0:ny+1, 0:nz+2)               :: rhow
  real, dimension(1:nx,1:ny+1, 1:nz)                   :: v 
  real, dimension(0:nx+1,0:ny+2, 0:nz+1)               :: rhov
  real                              :: x,y,z,tbar, pbar, xd, yd,zd, rad,rand
  real, dimension(1:2), intent(in)                              ::x_0,y_0, z_0,xrad,yrad, zrad 
  real, intent(in)                              :: uper
  integer :: i,j,k,m
  real    :: pi,r,d

pi = 4.0*atan(1.0)
! Loop Generating 'theta'
  do i = 1,nx
       do j = 1,ny
          do k = 1,nz
          x = 0.5*dx + dx*real(i-1)
          y = 0.5*dy + dy*real(j-1)           
          z = 0.5*dz + dz*real(k-1)           
          theta(i,j,k) = 300.0
          v(i,j,k)     = 0.0
          do m= 1,2
             xd = (x - x_0(m))
             yd = (y - y_0(m))
             zd = (z - z_0(m))
             rad = sqrt((xd/xrad(m))**2 + (yd/yrad(m))**2+ (zd/zrad(m))**2)
             if (rad.lt.1.0) then
             theta(i,j,k) = theta(i,j,k)+(tper(m,1))*0.5*(cos(rad*pi)+1.0)
             !v(i,j,k) = v(i,j,k)+(vper(m,1))*0.5*(cos(rad*pi)+1)
             end if
          end do
         enddo
 enddo
enddo

! rho at u/theta levels
do k = 0,nz+1
    z = 0.5*dz + dz*real(k-1)           
    tbar  = 300.0 - 9.81*z/1004.0 
    pbar = (10**5)*((tbar/300)**(1004/287))
    rhou(:,:,k) = pbar/(287*tbar)
enddo
        
! rho at z levels
   do k = 1,nz+1
          rhow(:,:,k) = 0.5*(rhou(:,:,k-1) + rhou(:,:,k)) 
    enddo
rhow(:,:,0) = 1000
rhow(:,:,nz+1) = 1000

rhow(:,:,0) = rhow(:,:,1)
rhow(:,:,nz+1) = rhow(:,:,nz)

! TODO: CONFIRM VALUE FOR RHOV

!call srand(0.0)
!!do k = 1,nz
!  do j = 1,ny
!     do i = 1,nx+1
!     u(i,j,k) = (rand(0) - 0.5)*uper
!      end do
!   end do
!end do

u = 0.0
v = 0.0
w = 0.0
p = 0.0

return
  end subroutine ic
