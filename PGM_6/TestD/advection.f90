! ======================= advection ===================
! Integrate forward (advection only) by one time step.
! ATMS 502 / CSE 566, Spring 2016
!
! Arguments:
!
!   theta1	           input    real array   values at current time step
!   q2	           output   real array   values at next time step
!   c              input    real         flow speed for linear case
!   dx,dz          input    real         grid spacing
!   dt             input    real         time step
!   nx,nz             input    integer	 number of grid points
!   advection_type input    character    'L' if linear, 'N' if nonlinear
!   u,v            input    real array   values at current ts

  subroutine  advection(theta1,theta2,u1,u2,u3,v1,v2,v3,w1,w2,w3, &
                nx,ny,nz,dx,dy,dz,tstep,dt,advection_type, gpoints)
  implicit none

  integer, intent(in)                               :: nx,ny,nz, gpoints
  real, intent(in)                                  :: dx,dy,dz,dt, tstep
  real, dimension(0:nx+1,0:ny+1,0:nz+1), intent(inout)     :: theta1, theta2
  real, dimension(0:nx+1,0:ny+1,0:nz+1)             :: theta_dummy
  character, intent(in)                             :: advection_type
  real, dimension(0:nx+2,0:ny+1, 0:nz+1), intent(inout)         :: u1,u2, u3
  real, dimension(0:nx+1, 0:ny+2,0:nz+1), intent(inout)         :: v1, v2,v3
  real, dimension(0:nx+1,0:ny+1,0:nz+2), intent(inout)         :: w1, w2,w3
  real, dimension((1-gpoints):(nx+gpoints))         :: q_1d, q2_1d
  real, dimension((1-gpoints):(ny+gpoints))         :: q_1dy, q2_1dy
  real, dimension((1-gpoints):(nz+gpoints))         :: q_1dz, q2_1dz
  real, dimension(1:nx+1)                           :: vel_1d
  real, dimension(1:nz+1)                           :: vel_1dz
  real, dimension(1:ny+1)                           :: vel_1dy
  integer                                           :: i,j,k


! Theta ADVECTION
  ! 1D advection in x 

theta_dummy = theta1    ! theta_dummy is a dummy arg for P to pass to BC
call bcp6(theta1, u1, v1,w1, theta_dummy,nx, ny,nz, gpoints)
 do j = 1,ny 
   do k = 1,nz
     q_1d = theta1(:,j,k)
     vel_1d(1:nx+1) = u1(1:nx+1,j,k)
 ! 1D Advection using advect1d   
     call  advect1d(q_1d,q2_1d,vel_1d,dt,dx,nx,advection_type, gpoints)
     theta2(1:nx,j,k) = q2_1d(1:nx)
  enddo
enddo

!call bcp6(theta2, u1,v1, w1, theta_dummy, nx, ny,nz, gpoints)

!!!! 1D advection in z for the entire q matrix
!   do i = 1,nx
!     do j = 1,ny
!     q_1dz = theta2(i,j,:)
!     vel_1dz(1:nz+1) = w1(i,j,1:nz+1)
!!! 1D advection using advect1d
!     call advect1d(q_1dz,q2_1dz,vel_1dz,dt,dz,nz,advection_type, gpoints)
!     theta2(i,j,1:nz) = q2_1dz(1:nz)
!  enddo
!enddo


!!call bcp6(theta2, u1,v1, w1, theta_dummy, nx, ny,nz, gpoints)
! ! 1D advection in y for the entire q matrix
!  do i = 1,nx
!     do k = 1,nz
!     q_1dy = theta2(i,:,k)
!     vel_1dy(1:ny+1) = v1(i,1:ny+1,k)
! !1D advection using advect1d
!     call advect1d(q_1dy,q2_1dy,vel_1dy,dt,dy,ny,advection_type, gpoints)
!     theta2(i,1:ny,k) = q2_1dy(1:ny)
!     enddo
!enddo
!
call bcp6(theta2, u2,v2, w2, theta_dummy,nx, ny,nz, gpoints)

!!! U advection
!do i = 1,nx +1
! do k = 1,nz
!  do j = 1,ny
! u3(i,j,k) = u3(i,j,k) + tstep*(-(u2(i+1,j,k)**2-u2(i-1,j,k)**2)/(2*dx) - & 
!            ((w2(i,j,k+1)+w2(i-1,j,k+1))*(u2(i,j,k+1) - u2(i,j,k))/(2*dz) + &
!            (w2(i,j,k) + w2(i-1,j,k))*(u2(i,j,k)-u2(i, j,k-1))/(2*dz))- &
!            (v2(i,j+1,k) + v2(i-1,j+1,k)*(u2(i,j+1,k) - u2(i,j,k))/(2*dy) + &
!            (v2(i,j,k) + v2(i-1,j,k))*(u2(i,j,k) -u2(i,j-1,k))/(2*dy)))/2 
! enddo
!enddo
!enddo
!
!!W  Advection
!do i = 1,nx
!  do k = 1, nz+1
!   do j = 1,ny
!    w3(i,j,k) = w3(i,j,k) - tstep*((u2(i+1,j,k)+ u2(i+1,j, k-1))*(w2(i+1,j,k) - &
!             w2(i,j,k))/(2*dx) + (u2(i,j,k) + u2(i,j,k-1))*(w2(i,j,k) -w2(i-1,j,k))/(2*dx)+&
!             (w2(i,j,k+1)**2 - w2(i,j,k-1)**2)/(2*dz) + & 
!             (v2(i,j+1,k)+v2(i,j+1,k-1))*(w2(i,j+1,k) - w2(i,j,k))/(2*dy) + & 
!             (v2(i,j,k) +v2(i,j,k-1))*(w2(i,j,k) - w2(i,j-1,k))/(2*dy))/2 
!    enddo
!  enddo
!enddo
!
!! V ADVECTION
!
!do i = 1,nx
!  do k = 1, nz
!   do j = 1,ny+1
!    v3(i,j,k) = v3(i,j,k) - tstep*((u2(i+1,j,k)+ u2(i+1,j-1, k))*(v2(i+1,j,k) - &
!            v2(i,j,k))/(2*dx) + (u2(i,j,k) + u2(i,j-1,k))*(v2(i,j,k) -v2(i-1,j,k))/(2*dx)&
!            +(v2(i,j+1,k)**2 - v2(i,j-1,k)**2)/(2*dy) + & 
!             (w2(i,j,k+1)+w2(i,j-1,k+1))*(v2(i,j,k+1) - v2(i,j,k))/(2*dz) + & 
!             (w2(i,j,k) +w2(i,j-1,k))*(v2(i,j,k) - v2(i,j,k-1))/(2*dz))/2 
!    enddo
!  enddo
!enddo

!call bcp6(theta2, u3,v3, w3, theta_dummy, nx, ny,nz, gpoints)
  return
  end subroutine advection
