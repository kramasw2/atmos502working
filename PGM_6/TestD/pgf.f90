
! ======================= pgf ===================
! Incorporates Pressure Gradient Forces/Buoyancy within
! the 2D Quasi-Compressible Advection/Diff eqns

! UPDATE DESCRIPTION IF POSSIBLE

! ATMS 502 / CSE 566, Spring 2016
!
! Arguments:
!
!   q1	           input    real array   values at current time step
!   q2	           output   real array   values at next time step
!   c              input    real         flow speed for linear case
!   dx,dy          input    real         grid spacing
!   dt             input    real         time step
!   nx,ny             input    integer	 number of grid points
!   advection_type input    character    'L' if linear, 'N' if nonlinear
!   u,v            input    real array   values at current ts

  subroutine pgf(theta1,u3,v3,w3,p1,p3,nx,ny,nz,dx,dy,dz,tstep, rhou, rhow, gpoints, cs)
   
  implicit none

  integer, intent(in)                               :: nx,ny,nz, gpoints
  real, intent(in)                                  :: dx,dy,dz, tstep
  real, dimension(0:nx+1,0:ny+1,0:nz+1), intent(inout)     :: p1,p3
  real, dimension(0:nx+2, 0:ny+1,0:nz+1), intent(inout)      :: u3
  real, dimension(0:nx+2, 0:ny+1,0:nz+1), intent(in)         :: rhou
  real, dimension(0:nx+1, 0:ny+2,0:nz+1), intent(inout)      :: v3
  real, dimension(0:nx+1, 0:ny+1,0:nz+2), intent(inout)      :: w3
  real, dimension(0:nx+1, 0:ny+1,0:nz+2), intent(inout)         :: rhow
  real, dimension(0:nx+1, 0:ny+1,0:nz+1)                   :: theta1
  real, intent(in)                                   :: cs
  integer                                           :: i,j,k

 call bcp6(theta1, u3, v3, w3, p1, nx, ny, nz, gpoints)

! U PGF terms
 do i = 1,nx+1
    do k = 1,nz
      do j = 1,ny
        u3(i,j,k) = u3(i,j,k) - tstep*(p1(i,j,k)-p1(i-1,j,k))/(rhou(i,j,k)*dx) 
    enddo
  enddo
enddo

! W PGF terms
do i = 1,nx
   do k = 1,nz+1
        do j = 1,ny
 w3(i,j,k) = w3(i,j,k) - tstep*(1/(rhow(i,j,k))*(p1(i,j,k)-p1(i,j,k-1))/(dz))+ & 
tstep*9.81*(theta1(i,j,k)+theta1(i,j,k-1)-600)/(2*300)
        enddo
    enddo
enddo

! V PGF terms
 do i = 1,nx
    do k = 1,nz
      do j = 1,ny+1
        v3(i,j,k) = v3(i,j,k) - tstep*(p1(i,j,k)-p1(i,j-1,k))/(rhou(i,j,k)*dy) 
    enddo
  enddo
enddo
 
! Set u,v,w BCs
 call bcp6(theta1, u3,v3,w3, p1, nx, ny, nz, gpoints)

! Pressure Equation
 do i = 1,nx
    do k = 1,nz
        do j = 1,ny
        p3(i,j,k) = p1(i,j,k) - tstep*(cs**2)*(rhou(i,j,k)*(u3(i+1,j,k) - u3(i,j,k))/(dx)+&
        rhou(i,j,k)*(v3(i,j+1,k) - v3(i,j,k))/(dy)+  &
        ((rhow(i,j,k+1))*w3(i,j,k+1)-(rhow(i,j,k))*w3(i,j,k))/dz)
        enddo
   enddo
enddo

call bcp6(theta1, u3,v3,w3, p3, nx,ny,nz,gpoints)
  return
  end subroutine pgf
