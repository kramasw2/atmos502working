! ============================ ic =====================
! IC sets the initial condition
! ATMS 502 / CSE 566, Spring 2016
!
! Arguments:
!
!   q1  output  real array   holds initial condition
!   dx  input   real         grid spacing
!   nx  integer              number of grid points

  subroutine ic(q1,dx,nx)
  implicit none

  integer, intent(in)                  :: nx
  real, intent(in)                     :: dx
  real, dimension(0:nx+1), intent(out) :: q1

  integer :: i
  real    :: x,pi,length

  pi = 4.0*atan(1.0)
  length = dx*real(nx)

  do i = 1,nx
    x = dx*real(i-1)
    q1(i) = sin( 2.*pi/length*x )
  enddo

  return
  end subroutine ic
