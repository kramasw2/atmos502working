!
!  ATMS 502 / CSE 566 -- Spring, 2016
!  Program 5:  Linear and nonlinear advection
!  >>>>> Karthik Ramaswamy <<<<<
!
  program pgm5
  implicit none

! ... Definitions ............................................................
! "nx" is the number of physical grid points in x - Not including 'ghost points'
! "nz" is the number of physical grid points in y - Not including 'ghost points' 
! "maxstep" is the maximum number of time steps.
! "c" is [constant] flow speed (e.g. meters/sec) - used only in linear case
! "dx" is grid spacing: distance (e.g. meters) between grid points in X, Y
! "name" is a variable containing your name - used to label the plots.
!"qt" is the true solution (initial condition)
! "qd" is the approximated solution (either L.W or Upstream)
! "gpoints" is the number of ghostpoints passed to advection
! 
  integer, parameter      :: nx=201
  integer, parameter      :: nz=41
  integer, parameter      :: maxstep=1000
  integer, parameter      :: gpoints = 3
  real, parameter         :: c=1.0
  real, parameter         :: dx=100
  real, parameter         :: dz=100
  character*30, parameter :: name="Karthik Ramaswamy"
! ... Other variables

  real    :: dt,courant,qmax, qmin, rtime, etot, ediss, edisp, rho, pi,angh, angvi, tstep
  integer      :: i,j,k,n,nplot, nstep
  character*1  :: reply,advection_type, label
! ... Arrays


! Main array dimensions are assigned including GP lengths.
  real, dimension(1:nx,1:nz)            :: q1,qt,qd
  real, dimension(0:nx+2,0:nz+1)        :: u1,u2, u3 ,rhou
  real, dimension(0:nx+1,1:nz+1)        :: w1, w2, w3, rhow
  real, dimension(0:nx+1,0:nz+1)        :: theta1, theta2, p1, p2, p3
  real, dimension(maxstep)    :: strace(maxstep) 
  real,dimension(1:maxstep)     :: qmin_ar, qmax_ar
!  real, dimension(nx,maxstep) :: history(nx,maxstep)

! ... Start, print, input ...........................................

  print*,'Program #5    Numerical Fluid Dynamics, Spring 2016'
  print*,' '

  print*,'Enter Desired Time step:'
  read*, dt
  print*,'Enter number of time steps to take:'
  read*,nstep

  print*,'Enter L for Linear  Lax-Wendroff, C for 6th Order Crowley, T for Takacs  advection:'
  read*,advection_type

  
! ... Open the NCAR Graphics package and set colors.
! ... To invert black/white colors, comment out calls to gscr below.

   call opngks
   call gscr(1,0,1.,1.,1.)
   call gscr(1,1,0.,0.,0.)

! ! ... X-axis label

!   call anotat('I','Q',0,0,0,0)

! ... Set default Y plot bounds.
 
!  call agsetf('Y/MINIMUM.',-1.2)
!  call agsetf('Y/MAXIMUM.', 1.2)

! ... Set and plot the initial condition. Note no ghost points passed to plot1d!

  call ic(theta1,dx,dz,nx,nz,u1,w1,p1,rhou, rhow)
  call ic(theta1(1:nx,1:nz),dx,dz,nx,nz, &
  u1(1:nx+1,1:nz),w1(1:nx,1:nz+1),p1(1:nx,1:nz),rhou(1:nx+1,1:nz), rhow(1:nx,&
1:nz+1))

! Initial condition plots as contrs
  call contr(theta1(1:nx,1:nz),nx,nz,2.0, rtime,'Initial Condition',0,.false.,0,0,0,0,name)

!  call contr(u,nx+1,nz,0.1, rtime,'Initial Condition',0,.false.,0,0,0,0,name)
!  call contr(w,nx,nz+1,0.1, rtime,'Initial Condition',0,.false.,0,0,0,0,name)
!  call sfc(q1,nx,nz,rtime,angh,angv,label,name)
! This copies the initial condition to the qt array 
! qt = q_plot
 !call stats(q1,nx,nz,0,qmax,qmin)

!angh = -45.0
!angv =  20.0

! Before calling integrate routine, do:

call bc(theta1, u1, w1, p1,nx, nz, gpoints)
tstep = 2*dt

! ... Integrate .....................................................
 !  do n = 1,nstep
  
!!   Set boundary conditions (B.C.s set in advection.f90)
!     call bc(q1,nx)
   u3 = u1
   w3 = w1
   theta2 = theta1
!   call pgf()
 !   Compute values at next step - Calling the advection routine
!    call  advection(q1,u,w,nx,nz,dx,dy,dt,advection_type, gpoints)
 !   Do array update at end of time step: This is done within advection.f90.
     !call update(q1,q2,nx)
! Call contr to plot the current q returned from advection
!       rtime = dt*real(n) 
!       if (mod(n,nplot).eq.0) then
!           call contr(q1,nx,nz,2.0, rtime,'Q field at time T',0,.false.,0,0,0,0,name)
!           call sfc(q1,nx,nz,rtime,angh,angv,label,name)
!        endif
!    Plot fields when needed.  Again only interior (1:nx) values are plotted.
     !if (n .eq. nstep) then		! code is done; plot final results
!       if (advection_type == 'L') then
!         call contr(q(1:nx),nx,n,qmax,.true. ,qtrue(1:nx),'Final solution',name)
!       else
! !        call plot1d(q2(1:nx),nx,n,qmax,.false.,qtrue(1:nx),'Final solution',name)
!       endif
!      if (mod(n,nplot).eq.0) then	! intermediate plot before end
! !      call plot1d(q2(1:nx),nx,n,qmax,.false.,qtrue(1:nx),'Solution',name)
!     endif

! !   Stats - to extract qmax and qmin
! call stats(q1,nx,nz,n,qmax, qmin)
!     strace(n) = qmax
! qmin_ar(n) = qmin
! qmax_ar(n) = qmax
! !   Check if problem out of bounds i.e. failing.
!    if (qmax.gt.35) then
!       print*,'Stop - solution blowing up at step',n
!      call plot1d(q2(1:nx),nx,n,qmax,.false.,qtrue(1:nx),'Solution blowing up',name)
!       nstep = n
!       exit
!    endif

! !   end of time loop from n=1 to nstep
!  enddo

! Stores the final solution in qd to pass to the takacs erroranalysis function, erroral
 !qd = q1
 
! call  erroral(qd,qt,nx, nz, etot, ediss, edisp, rho)
!print*,'Total Error = ',etot
!print*,'Dissipation Error =', ediss
!print*,'Dispersion Error=', edisp
!print*,'Max correlation=', rho

! ! ... Run complete - do final plots .................................

! ! ... Plot Qmax(t)


!call ezy(qmin_ar(1:nstep),n,'Min Q. vs. t')
!call ezy(qmax_ar(1:nstep),n,'Max Q. vs. t')

!print*,'Plotting surface.'
!!write(label,'(''Surface plot at n = '',i2)') n

! !  call agsetf('Y/MINIMUM.',0.)
! !  call agsetf('Y/MAXIMUM.',1.5)
! !  call anotat('N','Max value',0,0,0,0)
! !  call plot1d(strace,nstep,0,qmax,.false.,qtrue(1:nx),'Qmax vs. time',name)

! ! ... Plot history (time-space) surface s(x,t)

! !  call sfc(history,nx,nstep,dt*real(nstep),-90., 0.,    &
! !      	   'Time-Space evolution (view: -90,  0)',name)
! !  call sfc(history,nx,nstep,dt*real(nstep),-75., 5.,    &
! !      	   'Time-Space evolution (view: -75, +5)',name)
! !  call sfc(history,nx,nstep,dt*real(nstep),-30.,20.,    &
! !      	   'Time-Space evolution (view: -30,+20)',name)

! ! ... Close graphics package and stop.

  call clsgks
  stop
  end program pgm5
