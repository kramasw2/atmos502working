
! ======================= pgf ===================
! Incorporates Pressure Gradient Forces/Buoyancy within
! the 2D Quasi-Compressible Advection/Diff eqns

! UPDATE DESCRIPTION IF POSSIBLE

! ATMS 502 / CSE 566, Spring 2016
!
! Arguments:
!
!   q1	           input    real array   values at current time step
!   q2	           output   real array   values at next time step
!   c              input    real         flow speed for linear case
!   dx,dy          input    real         grid spacing
!   dt             input    real         time step
!   nx,ny             input    integer	 number of grid points
!   advection_type input    character    'L' if linear, 'N' if nonlinear
!   u,v            input    real array   values at current ts

  subroutine pgf(theta2,u3,w3,p1,p3,nx,nz,dx,dz,dt,tstep, rhou, rhow)
   
  implicit none

  integer, intent(in)                               :: nx,nz
  real, intent(in)                                  :: dx,dz,dt, tstep
  real, dimension(0:nx+1,0:nz+1), intent(inout)     :: p1,p3
  real, dimension(0:nx+2, 0:nz+1), intent(inout)      :: u3
  real, dimension(1:nx+1, 1:nz), intent(in)         :: rhou
  real, dimension(1:nx, 1:nz+1), intent(inout)      :: w3
  real, dimension(0:nx, 1:nz+1), intent(in)         :: rhow
  real, dimension(0:nx+1, 0:nz+1)                   :: theta2
  integer                                           :: i,k

 do i = 1,nx+1
    do k = 1,nz
 u3(i,k) = u3(i,k) - tstep*(1/rhou(i,k))*(p1(i+1,k)-p1(i-1,k))/(2*dx) 
    enddo
enddo

do i = 1,nx
   do k = 1,nz+1
 w3(i,k) = w3(i,k) - tstep*(1/rhou(i,k))*(p(i,k+1)-p1(i,k-1))/(2*dz)+ & 
 9.81*theta2(i,k)/(0.5*(theta2(i,k-1) + theta2(k+1))
   enddo
enddo
 
! Set u,w BCs
 call bc(theta2, u3, w3, p1, nx, nz, gpoints)
 do i = 1,nx
    do k = 1,nz
 p3 = p1+ tstep*(-(cs**2)*(rhou(i,k)*(u3(i+1,k) - &
u3(i-1,k))/(2*dx)+(rhow(i,k+1)*w3(i,k+1)-rhow(i,k-1)*w3(i,k-1))/(2*dz)))
    enddo
enddo

  return
  end subroutine pgf
