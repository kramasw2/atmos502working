#!/bin/csh
#
# Configure model viewer for your data. 
# Run make_images before this script.
# ATMS 391 / ATMS 502 / CS 505 / CSE 566
# Brian Jewett, Univ. Illinois Atmospheric Sciences
# ======================================================================
#                 MODEL SIMULATION VIEWER, beta11, 10/2014
# University of Illinois ... Atmospheric Sciences ... by Brian F. Jewett
# This is a partial conversion of a page used for other work.  If
# you wish to collaborate on improving this script or wish to get the
# latest version, contact me.  More information:  bjewett@illinois.edu
# ======================================================================

set ver      = 'beta11,  8 Oct 2014: UI Convective Modeling Group'
set rm_gifs  = yes	# remove GIFs once they are in archive file

#
# Necessary files
#

set www_template = viewer_template.shtml
if (! -e $www_template) then
  echo Error - web template file missing: $www_template
  echo Please copy it to the same directory from which this script is run.
  exit 1
endif

#
# Begin
#

if (`echo "$1" | cut -c 1-4` == help) then
  echo 'Usage:  make_viewer  [optional "zip", or "tar=filename"] ["optional title in quotes"]'
  exit
endif

echo Model viewer, "$ver"

#
# If we are to make a tar or zip file, check that prior one doesn't exist (or that we can rename it first)
#

setenv TAR 1				# default:  make a .tar file.
unsetenv ZIP
# if user asked for a tar file ...
set arg_len = `echo "$1" | wc -c`; @ arglen --

if ($arg_len == 0 || `echo "$1" | cut -c 1-3` == tar) then
  setenv TAR 1
  if (`echo $1 | cut -c 4` == '=') then
    set archive_file = `echo $1 | cut -c 5-`
  endif
endif
# if user asked for a zip file ...
if (`echo "$1" | cut -c 1-3` == zip) then
  unsetenv TAR
  setenv ZIP 1
  if (`echo $1 | cut -c 4` == '=') then
    echo zip=filename not working, please try tar.
    exit 1
    set archive_file = `echo $1 | cut -c 5-`
  endif
endif

# Find last GIF (last time, and alphabetically last in list)

set list       = `ls | grep '.gif' | grep 0 | tail -1`
set last_field = `echo $list | cut -d . -f 1 | cut -d _ -f 1`

# If "tar" or "zip" requested but no file name, create one based
# on the name of the GIFs.

if (`echo "$1" | cut -c 4` != '=') then
  if ($?TAR) then
    set archive_file = $last_field.tar
  else
    set archive_file = $last_field.zip
  endif
endif

# Blow out or rename old .zip or .tar file if needed.
# Prior renamed to the_old_name.old

if ($?TAR || $?ZIP) then
  if (-e $archive_file) then
    if (-e $archive_file.old) rm -f $archive_file.old
    mv $archive_file $archive_file.old
  endif
endif

unsetenv SKIP

echo Building list of model fields and times ...

#
# Build text file to hold the drop-down menus for each model time and field.
# Find time interval by (1) finding last file, (2) getting field name, (3) finding all times
#

set times_file      = "make_viewer.time.list"
set fields_file     = "make_viewer.field.list"
set menu_init_file  = "make_viewer.menu_init.list"

if (-e $times_file    ) rm $times_file
if (-e $fields_file   ) rm $fields_file
if (-e $menu_init_file) rm $menu_init_file

# Find last GIF (last time, and alphabetically last in list)

set list       = `ls | grep '.gif' | grep 0 | tail -1`
set last_field = `echo $list | cut -d . -f 1`

# List of all times for this field

set n = 0
set times = ""
foreach file (*${last_field}.*.gif)
  set model_time = `echo $file | cut -d . -f 2`
  set simple_time = `echo $model_time | sed 's/0*//'`
  if ($simple_time == "") set simple_time = 0
  if ($?SKIP) then
    if ($n > 0 && ($n % $SKIP != 0)) then
      @ n ++
      continue;
    endif
  endif
  set times = ( $times $model_time )
  if (! -e $times_file) then
    printf '<option value="%s">T=%s\n' $model_time $simple_time >! $times_file
  else
    printf '<option value="%s">T=%s\n' $model_time $simple_time >> $times_file
  endif
  @ n ++
end
echo $#times times: $times[1] - $times[$#times]

#
# Read in desired display preferences
# If "www_preferences" exists, read it now.
#

if (-e www_preferences) then
  echo Reading from www_preferences file...
  source www_preferences
endif

#
# List of fields, from files matching final time
#

set list = `ls | grep $times[$#times].gif | grep -v all`

# Field list is from file listing.

set raw_fields = ""
foreach file ($list)
  set field = `echo $file | cut -d . -f 1`
  set raw_fields = ( $raw_fields $field )
end

if ($?WWW_LEFT && $?WWW_RIGHT) then
  set fields = $WWW_LEFT
  set fields = ( $fields $WWW_RIGHT )
  set n = 0
  while ($n < $#raw_fields)
    @ n ++
    if ($raw_fields[$n] == $WWW_LEFT || \
	$raw_fields[$n] == $WWW_RIGHT) continue;
    set fields = ( $fields $raw_fields[$n] )
  end
else
  set fields = ( $raw_fields )
endif

set n = 0
foreach field ($fields)
  @ n ++
  if ($n == 1) then
    printf '<option value="%s">%s\n' $field $field >! $fields_file
  else
    printf '<option value="%s">%s\n' $field $field >> $fields_file
  endif
end
echo $#fields fields: "$fields"

printf 'var first_plot%sp  = new Array("%s");\n' 1 $fields[1] >! $menu_init_file
foreach npanel (2 4 6 8 12)
  printf 'var first_plot%sp  = new Array(' $npanel >> $menu_init_file
  set i = 1; set ivar = 1;
  while ($i <= $npanel)
    if ($i > 1) echo -n ','			   >> $menu_init_file
    printf '"%s"' $fields[$ivar]		   >> $menu_init_file
    @ ivar ++;
    if ($ivar > $#fields) set ivar = 1;
    @ i ++
  end
  echo ');'					   >> $menu_init_file 
end

#
# Get the full name of the person running this script.
#

set list = `finger -s $user | tail -1`
set n = 1
while (`echo $list[$n] | cut -c 1-4` != "pts/")
  @ n ++
end
@ n --
set name = "$list[2-$n]"

#
# Did user provide a title?  If so, use it here
#

set n = 1
unsetenv title
while ( $n <= $#argv)
  set arg = `echo "$argv[$n]"`
  if (`echo $arg | cut -c 1-3` != "tar" && \
      `echo $arg | cut -c 1-3` != "zip") then
    setenv title '<h2 align=center style="margin-top:5px; margin-bottom:0">'"$argv[$n-$#argv]"'</h2>'
    break
  endif
  @ n ++
end
if (! $?title) then
  setenv title ""
endif

#
# Build the various model view pages
#

set n = $#times;
set time_step = `echo $times[$n] | sed 's/0*//'`
@ n --         ;
set time_remove = `echo $times[$n] | sed 's/0*//'`
set time_last = $times[$#times];

foreach npanel ( 2 4 6 8 12 )
  cpp -P -DNPANEL=$npanel 	    	\
 	 -DTIME_STEP=$time_step	    	\
 	 -DLAST_TIME=$time_last 	\
 	 -DDEFAULT_LEFT=. 		\
 	 -DDEFAULT_RIGHT=.		\
	 -DNAME="$name"			\
	 -DTITLE="$title"		\
	 -DTODAY="`/bin/date`"		\
     $www_template >! build_www.tmp
  set file = viewer${npanel}p.shtml
  tail -n +3 build_www.tmp >! $file
  rm build_www.tmp
  echo Created web page: $file
  if ($?TAR) then
    if (-e $archive_file) then
      tar rf $archive_file $file
    else
      tar cf $archive_file $file
    endif
  else if ($?ZIP) then
    zip $archive_file $file
  endif
end

#
# Append to the tar or zip file, if requested.
#

if ($?TAR || $?ZIP) then
  echo -n "Writing to ${archive_file}: "
  foreach field ($fields)
    echo -n "$field "
    foreach model_time ($times)
      if ($?TAR) then
        tar rf $archive_file $field.$model_time.gif
      else
        zip -q $archive_file $field.$model_time.gif
      endif
      if ($rm_gifs == yes) rm $field.$model_time.gif
    end
  end
  echo ' '
  set list = `ls -l $archive_file`
  set file_size = $list[5]
  set size_mb = `(echo scale=2; echo $file_size/1024/1024) | bc`
  printf "%s: %.2f MB\n" ${archive_file} $size_mb
endif

# Done

echo $0 completed.
