! ============================ ic =====================
! IC sets the initial condition
! ATMS 502 / CSE 566, Spring 2016
!
! Arguments:
!
!   q1  output  real array   holds initial condition
!   dx  input   real         grid spacing
!   nx  integer              number of grid points

  subroutine ic(theta,dx,dz,nx,nz,u,w,p,rhou, rhow)
  implicit none

  integer, intent(in)                          :: nx,nz
  real, intent(in)                             :: dx, dz
  real, dimension(1:nx, 1:nz), intent(out)     :: theta,p
  real, dimension(1:2,1)                         :: tper 
  real, dimension(1:nx+1, 1:nz), intent(out)     :: u, rhou
  real, dimension(1:nx, 1:nz+1)                 :: w, rhow
  real                                    :: x,z,tbar, pbar, xd, zd, rad
  real, parameter                              ::x_0 = 10050, z_0 = 2050
  real, parameter                              :: xrad = 4000, zrad = 1000
  integer :: i,j,k,m
  real    :: pi,r,d
  r = 0.165
  tper(1,1) = -25.0
  tper(2,1) = 0.
  pi = 4.0*atan(1.0)

! Loop Generating 'theta'
  do i = 1,nx
       do k = 1,nz
          x = 0.5*dx + dx*real(i-1)
          z = 0.5*dz + dz*real(k-1)           
          theta(i,k) = 300.0
          do m= 1,2
             xd = (x - x_0)
             zd = (z - z_0)
             rad = sqrt((xd/xrad)**2 + (zd/zrad)**2)
             if (rad.le.1.0) then
             theta(i,k) = theta(i,k)+(tper(m,1))*0.5*(cos(rad*pi)+1)
             end if
          end do
 enddo
 enddo


! rho at u/theta levels
do i = 1,nx+1
   do k = 1,nz
       x = dx*real(i-1)
       z = 0.5*dz + dz*real(k-1)           
       tbar  = 300.0 - 9.81*z/1004.0  ! Check this - lowest here is 275
       pbar = (10**5)*(tbar/300)**(1004/287)
       rhou(i,k) = pbar/(287*tbar)
   enddo
enddo
        
! rho at z levels
rhow(:,1) = 10000
rhow(:,nz+1) = 10000
do i=1,nx
   do k = 2,nz-1
          x = 0.5*x + dx*real(i-1)
          z = dz*real(k-1)
          rhow(i,k) = 0.5*(rhou(i,k-1) + rhou(i,k+1)) 
    enddo
enddo

theta = theta - 300.0          

u = 0.0
w = 0.0
p = 0.0

!!! Loop Generating 'u'
!do i = 1,nx+1
!    do j = 1,nz
!      x = (dx*real(i-1) -0.5 - dx/2) 
!!      x = (dx*real(i-1) -0.5) 
!      z =  (dz*real(j-1) -0.5)
!      u(i,j) = (sin(4*pi*x)*sin(4*pi*z))
!    enddo
!enddo
!
!! Loop Generating 'w'
!do i = 1,nx
!   do j = 1,nz+1
!      x = (dx*real(i-1) -0.5) 
!      z =  (dz*real(j-1) -0.5 - dz/2)
!     w(i,j) = cos(4*pi*x)*cos(4*pi*z)
!   enddo
!enddo
!
!q_plot = q1(1:nx,1:nz)
return
  end subroutine ic
