! ============================ bc =====================
! BC sets the boundary conditions
! ATMS 502 / CSE 566, Spring 2016
!

! Arguments:
!
!   theta,p  in/output  real array    theta, p values
!   nx,nz	input      integer     main array size, not including
!                              extra 'ghost' zones/points
!   u,w         input      real arrays          main u,w arrays, size include gp
!
!
!
 
  subroutine bc(theta, u, w, p,nx, nz, gpoints)
  implicit none

  integer, intent(in)                            :: nx, nz, gpoints
!  real, dimension(1-gpoints:nx+gpoints, 1-gpoints:nz+gpoints), intent(inout) :: theta,p
!  real, dimension(1-gpoints:nx+1+gpoints, 1-gpoints: nz+gpoints) :: u
!  real, dimension(1-gpoints:nx+gpoints, 1-gpoints:nz+1+gpoints)  :: w

  real, dimension(0:nx+1, 0:nz+1), intent(inout) :: theta,p
  real, dimension(0:nx+2,0:nz+1),intent(inout)   :: u
  real, dimension(0:nx+1,1:nz+1),intent(inout)   :: w
  integer                                        :: i,k

! NOTE: This is coded so that no time is wasted in implementation. This must be
! changed to accomodate gpoints as a generalized version. 

!................Top, Bottom walls...................................!

! Zero gradient for u at top and bottom
do i = 1,nx +1
   u(i,nz+1) = u(i,nz)
   u(i,0) = u(i,1)
enddo

! Zero gradient for w at top and bottom
do i = 1,nx
   w(i,nz+1) = 0.0
   w(i,1) = 0.0
enddo

! Zero Gradient for theta, p

do i =1,nx
  theta(i,nz+1) = theta(i,nz)
  p(i,nz+1) = p(i,nz)
  theta(i,0) = theta(i,1)
  p(i,0) = p(i,1)
enddo

!............Left, right walls.........................................!

! Symmetry for p,theta,w

do k = 1,nz
p(0,k) = p(2,k)
p(nx+1,k) = p(nx-1,k)

theta(0,k) = theta(2,k)
theta(nx+1,k) = theta(nx-1,k)
enddo

do k = 1,nz+1
w(0,k) = w(2,k)
w(nx+1,k) = w(nx-1,k)
enddo

! Antisymmetry for u

do k = 1,nz
u(1,k) = -u(2,k)
u(0,k) = -u(3,k)
u(nx+1,k) = -u(nx,k)
u(nx+2,k) = -u(nx-1,k)
enddo

  return
end subroutine bc
