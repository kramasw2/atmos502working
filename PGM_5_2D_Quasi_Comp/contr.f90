!
! .............................. contr ............................
!
!  CONTR  contours the given field and shows the max/min & label, with box option for nesting.
!  ATMS 502/CSE 566
!  Brian Jewett   Spring 2016
!
!  Arguments:
!
!	z	input	real array	field to be contoured, size (nx,ny)
!	nx,ny	input	integers	dimensions of 'z'
!	cint	input	real		interval at which to draw contours
!	time	input	real		time of integration - real value
!	label	input	character*(*)	character label; should not
!					exceed 30 characters long.
!	colors	input	integer		if =0, positive values colored red, <0 blue;
!					if >0, colors reversed;
!					if <0, all colors black (but negative values dashed)
!	pltzero	input	logical		if true, the zero contour is plotted.
!					if false, zero contour is omitted.
!	i1,i2,	input	integers	bounding box to highlight on plot;
!	  j1,j2				  set i1=0 to skip (if i1=0, no box plotted)
!					  change box_thickness option below if desired.
!	name	input	character*(*)	name to place on plot
!
	subroutine contr(z,nx,ny,cint,time,label,colors, &
      	  pltzero,i1,i2,j1,j2,name)
	implicit none

	integer nx,ny,colors,i1,i2,j1,j2
	real z(nx,ny), cint, time
	character*(*) label,name
	logical pltzero

	integer liwk,lrwk
	parameter (liwk=3000,lrwk=5000)
	integer iwrk(liwk)
	real rwrk(lrwk)

	integer i,j,ncon,l,zimin,zimax,zjmin,zjmax,lss
	real cval,zmin,zmax,cmin,cmax,csize, &
      	  vx1,vx2,vy1,vy2,wx1,wx2,wy1,wy2
	character mmlabel*60,tlabel*15

! ... Internal controls
!        If true, debug produces text information
!        label_interval : 0=none, 1=all, 2=every other contour labeled, etc.
!        high_low_labels: 0=none; 1=H/L only; 2=H/L with value; 3=value only
!	 high_low_size  : set the size of the high/low labels
!	 cntr_smoothing : sets contour spline tension (0: none)
!	 box_thickness  : sets thickness of bounding box (1.0=standard line width)

	logical debug
	integer high_low_labels,label_interval
	real high_low_size,cntr_smoothing,box_thickness
	parameter (debug           = .false.)
	parameter (label_interval  = 0)
	parameter (high_low_labels = 2)
	parameter (high_low_size   = 0.025)
	parameter (cntr_smoothing  = 0.000)
	parameter (box_thickness   = 3.0)

! ... Find min, max

	zimin = 1
	zjmin = 1
	zimax = 1
	zjmax = 1
	zmin = z(1,1)
	zmax = zmin

	do j = 1,ny
	  do i = 1,nx
	    if (z(i,j).lt.zmin) then
	      zmin = z(i,j)
	      zimin = i
	      zjmin = j
	    endif
	    if (z(i,j).gt.zmax) then
	      zmax = z(i,j)
	      zimax = i
	      zjmax = j
	    endif
	  enddo
	enddo
	if (debug) print*,'Contr: ',nx,ny,zmin,zmax,': ',label

! ... Create min/max and time labels.

	if (zmin.eq.zmax) then
	  csize = 0.014
	  if (abs(zmin).lt.999.0) then
	    write(mmlabel,10) zmin
10	    format('CONSTANT FIELD = ',f10.5)
	    l = 27
	  else
	    write(mmlabel,15) zmin
15	    format('CONSTANT FIELD = ',e11.3)
	    l = 28
	  endif

	else if (zmin.gt.-999.0.and.zmax.lt.999.) then
	  csize = 0.014
	  write(mmlabel,20) zmin,zimin,zjmin, 	    &
      			    zmax,zimax,zjmax
20	  format('MIN =',f8.3,' (',i4,',',i3,'), ', &
      	         'MAX =',f8.3,' (',i4,',',i3,')')
	  l = 50

	else
	  write(mmlabel,25) zmin,zimin,zjmin,	     &
      			    zmax,zimax,zjmax
25	  format('MIN =',e11.3,' (',i4,',',i3,'), ', &
      	         'MAX =',e10.3,' (',i4,',',i3,')')
	  l = 55
	  csize = 0.013
	endif
!	write(tlabel,30)time
30	format('T=',f8.3)
	if (debug) then
      	  print*,'mmlabel ''',mmlabel,''''
      	  print*,' tlabel ''', tlabel,''''
	endif

! ... Plot labels

	call set(0.,1.,0.,1.,0.,1.,0.,1.,1)
	call pcmequ(0.50,0.970,  label,      0.020,0.0, 0.0);
	call pcmequ(0.95,0.030,mmlabel(1: l),csize,0.0, 1.0);
	call pcmequ(0.05,0.030, tlabel(1:10),0.014,0.0,-1.0);

!
! ... Additional labels
!
        call pcmequ(0.02,0.94,name,0.01,90.,1.)
        call pcmequ(0.98,0.06,'ATMS 502/CSE 566',0.01,90.,-1.)
        call pcmequ(0.98,0.94,'Spring, 2016',0.01,90.,1.)

! ... Set colors for contours

        call gscr(1,1,0.,1.,1.)
        call gscr(1,4,0.,0.,0.)
	if (colors == 0) then
          call gscr(1,1,1.,0.,0.)
          call gscr(1,2,0.,1.,0.)
          call gscr(1,3,0.,0.,1.)
	  if (debug) print*,'Positive values contoured red, negative blue.'
	else if (colors > 0) then
          call gscr(1,1,0.,0.,1.)
          call gscr(1,2,0.,1.,0.)
          call gscr(1,3,1.,0.,0.)
	  if (debug) print*,'Positive values contoured blue, negative red.'
	else
          call gscr(1,1,0.,0.,0.)
          call gscr(1,2,0.,0.,0.)
          call gscr(1,3,0.,0.,0.)
	  if (debug) print*,'All contours plotted black'
	endif

! ... Prepare to plot contours.

	call cpsetr("CIS - CONTOUR INTERVAL SPECIFIER",cint)
 	call cpseti("LIS - LABEL INTERVAL SPECIFIER"  ,label_interval)
 	call cpseti("HIC - High Label Color Index"    ,4)
 	call cpseti("LOC - Low Label Color Index"     ,4)
	if (high_low_labels .eq. 0) then
	  call cpsetc("HLT",'')
	else if (high_low_labels .eq. 1) then
	  call cpsetc("HLT",'H''L')
	else if (high_low_labels .eq. 2) then
	  call cpsetc("HLT",'H:B:$ZDV$:E:''L:B:$ZDV$:E:')
	else
	  call cpsetc("HLT",'$ZDV$''$ZDV$')
	endif
        call cpsetr('HLS - HIGH/LOW LABEL SIZE',high_low_size)
        call cpsetr('T2D - Tension 2-dim spline',cntr_smoothing)
	call cpseti('HLO - High/Low Label Overlap Flag',0)
!	call cpsetc('CFT','')
        call cpsetc('ILT','')
	if (.not.pltzero.and.debug) &
      	  print*,'Zero contour omitted; plotting positive values.'

! ... Plot positive (and possibly zero) contours.

	if ( (pltzero.and.zmax.ge.0.0) .or. 	  &
      	     (.not.pltzero.and.zmax.ge.cint) .or. &
      	     (zmin.eq.zmax.and.zmin.eq.0.0) ) then
	  cmin = cint * real( ifix(zmin/cint) )
	  if (cmin.lt.0.0) cmin=0.0
	  if (cmin.eq.0.0.and..not.pltzero) cmin=cint
	  call cpsetr("CMN - CONTOUR MIN",cmin)
	  cmax = max(cmin,cint * real( ifix(zmax/cint) ))
	  call cpsetr("CMX - CONTOUR MAX",cmax)
	  call cprect(z,nx,nx,ny,rwrk,lrwk,iwrk,liwk);
!	  call cpback(z,rwrk,iwrk)
	  call cppkcl(z,rwrk,iwrk)
	  call cpgeti("NCL",ncon)
	  if (debug) print*,'Positive contours to be plotted:',ncon

! ... Set color of each contour depending on <0,=0,>0

	  do i = 1,ncon
	    call cpseti("PAI",i)
	    call cpgetr("CLV",cval)
	    if (label_interval.eq.0) call cpseti("CLU",1)
	    if (cval .gt. 0.0) then
	      call cpseti("CLC",1)
	    else if (cval .eq. 0.0) then
	      call cpseti("CLC",2)
	    else
	      call cpseti("CLC",3)
	      if (colors<0) call cpseti("CLD",21845);
	    endif
	  enddo

	  call cplbdr(z,rwrk,iwrk)
	  call cpcldr(z,rwrk,iwrk)
	  call gacolr(4,4,4,4);
	  call gridal(nx-1,0,ny-1,0,0,0,5,0.,0.);
	endif

! ... Now plot all contours for negative values

	if (zmin .lt. 0.) then
	  call cpsetr("CIS - CONTOUR INTERVAL SPECIFIER",cint)
 	  call cpseti("LIS - LABEL INTERVAL SPECIFIER"  ,label_interval)
	  cmin = cint * real( ifix(zmin/cint) )
	  if (cmin.gt.(-cint)) cmin=(-cint)
	  if (zmax.ge.0.0) then
	    cmax = -cint
	  else
	    cmax = cint * real( ifix(zmax/cint) )
	  endif
	  call cpsetr("CMN - CONTOUR MIN",cmin)
	  call cpsetr("CMX - CONTOUR MAX",cmax)

	  call cprect(z,nx,nx,ny,rwrk,lrwk,iwrk,liwk);
!	  call c_cpback(z,rwrk,iwrk)
	  call cppkcl(z,rwrk,iwrk)
	  call cpgeti("NCL",ncon)
	  if (debug) print*,'Negative contours to be plotted:',ncon

	  do i = 1,ncon
	    call cpseti("PAI",i)
	    call cpgetr("CLV",cval);
	    if (label_interval.eq.0) call cpseti("CLU",1)
	    if (cval > 0.0) then
	      call cpseti("CLC",1)
	    else if (cval == 0.0) then
	      call cpseti("CLC",2)
	    else
	      call cpseti("CLC",3)
	      if (colors<0) call cpseti("CLD",21845)
	    endif
	  enddo

	  call cplbdr(z,rwrk,iwrk)
	  call cpcldr(z,rwrk,iwrk)
	  call gacolr(4,4,4,4)
	  call gridal(nx-1,0,ny-1,0,0,0,5,0.,0.)
	endif

! ... Plot bounding box if desired (if i1 is nonzero)

	if (i1.ne.0) then
	  call plotif(0.,0.,2)
	  call gslwsc(box_thickness)
	  call frstpt(real(i1),real(j1))
	  call vector(real(i2),real(j1))
	  call vector(real(i2),real(j2))
	  call vector(real(i1),real(j2))
	  call vector(real(i1),real(j1))
	  call plotif(0.,0.,2)
 	  call gslwsc(1.0)
	endif

! ... Close plot frame.

	call frame

! ... Restore colors and return.

        call gscr(1,1,0.,0.,0.)
        call gscr(1,2,0.,0.,0.)
        call gscr(1,3,0.,0.,0.)
        call gscr(1,4,0.,0.,0.)

	return
	end
