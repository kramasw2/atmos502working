! ========================= stats =====================
! Stats computes and prints out the max and min Q values
! ATMS 502 / CSE 566, Spring 2016
!
! Arguments:
!
!   q2	  input   real array   latest array values
!   nx	  input   integer      size of data array
!   n	  input   integer      time step counter, from main program
!   qmax, qmin  output  real         holds max absolute value of q2
!
    subroutine stats(q1,nx,ny,n,qmax,qmin)
    implicit none

    integer, intent(in)                 :: n,nx,ny
    real, intent(out)                   :: qmax,qmin
    real, dimension(1:nx, 1:ny), intent(in) :: q1

    integer :: i,j
   
    qmax = maxval(q1)
    qmin = minval(q1)
    write(6,1) n,qmax
    write(6,1) n,qmin
1   format('Step ',i3,', Max,Min = ',f9.4,f9.4)
    
    return

    end subroutine stats
